-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : sam. 04 fév. 2023 à 14:13
-- Version du serveur : 8.0.27
-- Version de PHP : 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `learning`
--

-- --------------------------------------------------------

--
-- Structure de la table `choixs`
--

DROP TABLE IF EXISTS `choixs`;
CREATE TABLE IF NOT EXISTS `choixs` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `choixs`
--

INSERT INTO `choixs` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Obligatoire', 1, '2022-11-03 10:01:15', '2022-11-03 10:01:15'),
(2, 'Facultatif', 1, '2022-11-03 10:01:15', '2022-11-03 10:01:15');

-- --------------------------------------------------------

--
-- Structure de la table `control`
--

DROP TABLE IF EXISTS `control`;
CREATE TABLE IF NOT EXISTS `control` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `univ_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sigle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slogan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nbre_mois` int NOT NULL DEFAULT '2',
  `closing_msg` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `close_site` tinyint(1) NOT NULL DEFAULT '0',
  `payment_unit` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `control`
--

INSERT INTO `control` (`id`, `univ_name`, `sigle`, `slogan`, `logo`, `favicon`, `email`, `phone`, `address`, `facebook`, `youtube`, `nbre_mois`, `closing_msg`, `close_site`, `payment_unit`, `created_at`, `updated_at`) VALUES
(1, 'e-ESIGE', 'ESIGE', '', '1652616359_logo-esige-mdf-1.png', '1652616378_logo-esige-mdf-1.png', 'esige@esige.mg', '', 'Tsararano ambony Mahajanga', 'http://www.facebook.com', 'http://www.youtube.com', 15, 'Ce site est en cours de maintenance.', 0, 'MGA', '0000-00-00 00:00:00', '2023-01-04 14:09:14');

-- --------------------------------------------------------

--
-- Structure de la table `elements`
--

DROP TABLE IF EXISTS `elements`;
CREATE TABLE IF NOT EXISTS `elements` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int UNSIGNED NOT NULL,
  `codeEc` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `codeUe` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `class_id` int UNSIGNED NOT NULL,
  `parcour_id` int NOT NULL,
  `semestre` int NOT NULL,
  `id_teacher` int NOT NULL,
  `al_value` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `abr` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `volH` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `coef` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `tronc` tinyint(1) NOT NULL DEFAULT '0',
  `choix` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=271 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `elements`
--

INSERT INTO `elements` (`id`, `user_id`, `codeEc`, `codeUe`, `class_id`, `parcour_id`, `semestre`, `id_teacher`, `al_value`, `name`, `abr`, `slug`, `volH`, `coef`, `status`, `tronc`, `choix`, `created_at`, `updated_at`) VALUES
(1, 2, 'EC1.1.1', 'UE1.1', 2, 2, 1, 5, 0, 'Fran&ccedil;ais', 'FRS', 'français', '14', '2', 1, 1, 0, '2022-12-09 10:07:39', '2023-01-07 21:23:41'),
(2, 2, 'EC1.1.1', 'UE1.1', 2, 3, 1, 0, 0, 'Fran&ccedil;ais', 'FRS', 'français', '14', '2', 1, 1, 0, '2022-12-09 10:07:39', '2022-12-12 09:27:57'),
(3, 2, 'EC1.1.1', 'UE1.1', 2, 4, 1, 0, 0, 'Fran&ccedil;ais', 'FRS', 'français', '14', '2', 1, 1, 0, '2022-12-09 10:07:39', '2022-12-12 09:47:33'),
(4, 2, 'EC1.1.1', 'UE1.1', 2, 5, 1, 0, 0, 'Fran&ccedil;ais', 'FRS', 'français', '14', '2', 1, 1, 0, '2022-12-09 10:07:39', '2022-12-12 10:11:30'),
(5, 2, 'EC1.1.1', 'UE1.1', 2, 6, 1, 0, 0, 'Fran&ccedil;ais', 'FRS', 'français', '14', '2', 1, 1, 0, '2022-12-09 10:07:39', '2022-12-12 10:43:02'),
(6, 2, 'EC1.1.2', 'UE1.1', 2, 2, 1, 3, 0, 'Anglais', 'ANG', 'anglais', '14', '2', 1, 1, 0, '2022-12-09 10:09:43', '2023-01-07 21:23:59'),
(7, 2, 'EC1.1.2', 'UE1.1', 2, 3, 1, 0, 0, 'Anglais', 'ANG', 'anglais', '14', '2', 1, 1, 0, '2022-12-09 10:09:43', '2022-12-12 09:31:36'),
(8, 2, 'EC1.1.2', 'UE1.1', 2, 4, 1, 0, 0, 'Anglais', 'ANG', 'anglais', '14', '2', 1, 1, 0, '2022-12-09 10:09:43', '2022-12-12 09:47:53'),
(9, 2, 'EC1.1.2', 'UE1.1', 2, 5, 1, 0, 0, 'Anglais', 'ANG', 'anglais', '14', '2', 1, 1, 0, '2022-12-09 10:09:43', '2022-12-12 10:11:55'),
(10, 2, 'EC1.1.2', 'UE1.1', 2, 6, 1, 0, 0, 'Anglais', 'ANG', 'anglais', '14', '2', 1, 1, 0, '2022-12-09 10:09:43', '2022-12-12 10:43:35'),
(11, 2, 'EC1.1.3', 'UE1.1', 2, 2, 1, 0, 0, 'Italien', 'ITL', 'italien', '14', '2', 1, 1, 0, '2022-12-09 10:49:38', '2022-12-12 08:49:01'),
(12, 2, 'EC1.1.3', 'UE1.1', 2, 3, 1, 0, 0, 'Italien', 'ITL', 'italien', '14', '2', 1, 1, 0, '2022-12-09 10:49:38', '2022-12-12 09:32:02'),
(13, 2, 'EC1.1.3', 'UE1.1', 2, 4, 1, 0, 0, 'Italien', 'ITL', 'italien', '14', '2', 1, 1, 0, '2022-12-09 10:49:38', '2022-12-12 09:48:16'),
(14, 2, 'EC1.1.3', 'UE1.1', 2, 5, 1, 0, 0, 'Italien', 'ITL', 'italien', '14', '2', 1, 1, 0, '2022-12-09 10:49:38', '2022-12-12 10:12:18'),
(15, 2, 'EC1.1.3', 'UE1.1', 2, 6, 1, 0, 0, 'Italien', 'ITL', 'italien', '14', '2', 1, 1, 0, '2022-12-09 10:49:38', '2022-12-12 10:43:59'),
(16, 2, 'EC1.2.1', 'UE1.2', 2, 2, 1, 0, 0, 'Economie g&eacute;n&eacute;rale', 'ECO GE', 'economie-générale', '14', '2', 1, 1, 0, '2022-12-09 10:51:26', '2022-12-12 08:49:22'),
(17, 2, 'EC1.2.1', 'UE1.2', 2, 3, 1, 0, 0, 'Economie g&eacute;n&eacute;rale', 'ECO GE', 'economie-générale', '14', '2', 1, 1, 0, '2022-12-09 10:51:26', '2022-12-12 09:32:31'),
(18, 2, 'EC1.2.1', 'UE1.2', 2, 4, 1, 0, 0, 'Economie g&eacute;n&eacute;rale', 'ECO GE', 'economie-générale', '14', '2', 1, 1, 0, '2022-12-09 10:51:26', '2022-12-12 09:48:42'),
(19, 2, 'EC1.2.1', 'UE1.2', 2, 5, 1, 0, 0, 'Economie g&eacute;n&eacute;rale', 'ECO GE', 'economie-générale', '14', '2', 1, 1, 0, '2022-12-09 10:51:26', '2022-12-12 10:12:41'),
(20, 2, 'EC1.2.1', 'UE1.2', 2, 6, 1, 0, 0, 'Economie g&eacute;n&eacute;rale ', 'ECO GE', 'economie-générale', '14', '2', 1, 1, 0, '2022-12-09 10:51:26', '2022-12-12 10:44:21'),
(21, 2, 'EC1.3.1', 'UE1.3', 2, 2, 1, 0, 0, 'Comptabilit&eacute; g&eacute;n&eacute;rale I', 'COMPTA GE', 'comptabilité-générale-i', '28', '4', 1, 1, 0, '2022-12-09 10:53:16', '2022-12-12 09:12:04'),
(22, 2, 'EC1.3.1', 'UE1.3', 2, 3, 1, 0, 0, 'Comptabilit&eacute; g&eacute;n&eacute;rale I', 'COMPTA GE', 'comptabilité-générale-i', '28', '4', 1, 1, 0, '2022-12-09 10:53:16', '2022-12-12 09:33:48'),
(23, 2, 'EC1.3.1', 'UE1.3', 2, 4, 1, 0, 0, 'Comptabilit&eacute; g&eacute;n&eacute;rale I', 'COMPTA GE', 'comptabilité-générale-i', '28', '4', 1, 1, 0, '2022-12-09 10:53:16', '2022-12-12 09:49:09'),
(24, 2, 'EC1.3.1', 'UE1.3', 2, 5, 1, 0, 0, 'Comptabilit&eacute; g&eacute;n&eacute;rale I', 'COMPTA GE', 'comptabilité-générale-i', '28', '4', 1, 1, 0, '2022-12-09 10:53:16', '2022-12-12 10:21:51'),
(25, 2, 'EC1.3.1', 'UE1.3', 2, 6, 1, 0, 0, 'Comptabilit&eacute; g&eacute;n&eacute;rale I', 'COMPTA GE', 'comptabilité-générale-i', '28', '4', 1, 1, 0, '2022-12-09 10:53:16', '2022-12-12 10:44:45'),
(26, 2, 'EC1.3.2', 'UE1.3', 2, 2, 1, 0, 0, 'Bureautique th&eacute;orie', 'BUR THEOR', 'bureautique-théorie', '14', '2', 1, 1, 0, '2022-12-09 11:05:01', '2022-12-15 09:57:30'),
(27, 2, 'EC1.3.2', 'UE1.3', 2, 3, 1, 0, 0, 'Bureautique th&eacute;orie', 'BUR THEOR', 'bureautique-théorie', '14', '2', 1, 1, 0, '2022-12-09 11:05:01', '2022-12-15 09:58:07'),
(28, 2, 'EC1.3.2', 'UE1.3', 2, 4, 1, 0, 0, 'Bureautique th&eacute;orie', 'BUR THEOR', 'bureautique-théorie', '14', '2', 1, 1, 0, '2022-12-09 11:05:01', '2022-12-15 09:58:40'),
(29, 2, 'EC1.3.2', 'UE1.3', 2, 5, 1, 0, 0, 'Bureautique th&eacute;orie', 'BUR THEOR', 'bureautique-théorie', '14', '2', 1, 1, 0, '2022-12-09 11:05:01', '2022-12-15 09:59:11'),
(30, 2, 'EC1.3.2', 'UE1.3', 2, 6, 1, 0, 0, 'Bureautique th&eacute;orie', 'BUR THEOR', 'bureautique-théorie', '14', '2', 1, 1, 0, '2022-12-09 11:05:01', '2022-12-15 09:59:59'),
(31, 2, 'EC1.3.3', 'UE1.3', 2, 2, 1, 0, 0, 'Technique de r&eacute;daction I', 'TECH REDAC', 'technique-de-rédaction-i', '14', '2', 1, 1, 0, '2022-12-09 11:10:49', '2022-12-14 14:03:31'),
(32, 2, 'EC1.3.3', 'UE1.3', 2, 3, 1, 0, 0, 'Technique de r&eacute;daction I', 'TECH REDAC', 'technique-de-rédaction-i', '14', '2', 1, 1, 0, '2022-12-09 11:10:49', '2022-12-15 10:02:08'),
(33, 2, 'EC1.3.3', 'UE1.3', 2, 4, 1, 0, 0, 'Technique de r&eacute;daction I', 'TECH REDAC', 'technique-de-rédaction-i', '14', '14', 1, 1, 0, '2022-12-09 11:10:49', '2022-12-12 09:50:21'),
(34, 2, 'EC1.3.3', 'UE1.3', 2, 5, 1, 0, 0, 'Technique de r&eacute;daction I', 'TECH REDAC', 'technique-de-rédaction-i', '14', '2', 1, 1, 0, '2022-12-09 11:10:49', '2022-12-15 10:06:48'),
(35, 2, 'EC1.3.3', 'UE1.3', 2, 6, 1, 0, 0, 'Technique de r&eacute;daction I', 'TECH REDAC', 'technique-de-rédaction-i', '14', '2', 1, 1, 0, '2022-12-09 11:10:49', '2022-12-15 10:07:57'),
(36, 2, 'EC1.4.1', 'UE1.4', 2, 2, 1, 0, 0, 'Initiation juridique', 'IJ', 'initiation-juridique', '14', '2', 1, 1, 0, '2022-12-09 11:13:23', '2022-12-12 09:15:59'),
(37, 2, 'EC1.4.1', 'UE1.4', 2, 3, 1, 0, 0, 'Initiation juridique', 'IJ', 'initiation-juridique', '14', '2', 1, 1, 0, '2022-12-09 11:13:23', '2022-12-12 09:36:07'),
(38, 2, 'EC1.4.1', 'UE1.4', 2, 4, 1, 0, 0, 'Initiation juridique', 'IJ', 'initiation-juridique', '14', '2', 1, 1, 0, '2022-12-09 11:13:23', '2022-12-12 09:50:49'),
(39, 2, 'EC1.4.1', 'UE1.4', 2, 5, 1, 0, 0, 'Initiation juridique', 'IJ', 'initiation-juridique', '14', '2', 1, 1, 0, '2022-12-09 11:13:23', '2022-12-12 10:28:51'),
(40, 2, 'EC1.4.1', 'UE1.4', 2, 6, 1, 0, 0, 'Initiation juridique', 'IJ', 'initiation-juridique', '14', '2', 1, 1, 0, '2022-12-09 11:13:23', '2022-12-12 10:46:10'),
(41, 2, 'EC1.4.2', 'UE1.4', 2, 2, 1, 0, 0, 'Droit des obligations', 'DT OBLI', 'droit-des-obligations', '14', '2', 1, 1, 0, '2022-12-09 11:14:30', '2022-12-12 09:16:30'),
(42, 2, 'EC1.4.2', 'UE1.4', 2, 3, 1, 0, 0, 'Droit des obligations', 'DT OBLI', 'droit-des-obligations', '14', '2', 1, 1, 0, '2022-12-09 11:14:30', '2022-12-12 09:36:41'),
(43, 2, 'EC1.4.2', 'UE1.4', 2, 4, 1, 0, 0, 'Droit des obligations', 'DT OBLI', 'droit-des-obligations', '14', '2', 1, 1, 0, '2022-12-09 11:14:30', '2022-12-12 10:00:02'),
(44, 2, 'EC1.4.2', 'UE1.4', 2, 5, 1, 0, 0, 'Droit des obligations', 'DT OBLI', 'droit-des-obligations', '14', '2', 1, 1, 0, '2022-12-09 11:14:30', '2022-12-12 10:29:32'),
(45, 2, 'EC1.4.2', 'UE1.4', 2, 6, 1, 0, 0, 'Droit des obligations', 'DT OBLI', 'droit-des-obligations', '14', '2', 1, 1, 0, '2022-12-09 11:14:30', '2022-12-12 10:46:34'),
(46, 2, 'EC1.4.3', 'UE1.4', 2, 2, 1, 0, 0, 'Droit civil', 'DT CIV', 'droit-civil', '14', '2', 1, 1, 0, '2022-12-09 11:17:55', '2022-12-12 09:16:59'),
(47, 2, 'EC1.4.3', 'UE1.4', 2, 3, 1, 0, 0, 'Droit civil', 'DT CIV', 'droit-civil', '14', '2', 1, 1, 0, '2022-12-09 11:17:55', '2022-12-12 09:38:01'),
(48, 2, 'EC1.4.3', 'UE1.4', 2, 4, 1, 0, 0, 'Droit civil', 'DT CIV', 'droit-civil', '14', '2', 1, 1, 0, '2022-12-09 11:17:55', '2022-12-12 10:00:34'),
(49, 2, 'EC1.4.3', 'UE1.4', 2, 5, 1, 0, 0, 'Droit civil', 'DT CIV', 'droit-civil', '14', '2', 1, 1, 0, '2022-12-09 11:17:55', '2022-12-12 10:30:05'),
(50, 2, 'EC1.4.3', 'UE1.4', 2, 6, 1, 0, 0, 'Droit civil', 'DT CIV', 'droit-civil', '14', '2', 1, 1, 0, '2022-12-09 11:17:55', '2022-12-12 10:47:14'),
(51, 2, 'EC1.5.1', 'UE1.5', 2, 2, 1, 0, 2, 'Initiation en tourisme', 'I TOUR', 'initiation-en-tourisme', '14', '2', 1, 0, 0, '2022-12-09 11:21:28', '2022-12-12 09:17:40'),
(52, 2, 'EC1.5.2', 'UE1.5', 2, 2, 1, 0, 2, 'Amanagement touristique I', 'AM TOUR', 'amanagement-touristique-i', '14', '2', 1, 0, 0, '2022-12-09 11:22:26', '2022-12-12 09:18:18'),
(53, 2, 'EC1.6.1', 'UE1.6', 2, 2, 1, 0, 2, 'Initiation au marketing', 'I MARKT', 'initiation-au-marketing', '14', '2', 1, 0, 0, '2022-12-09 11:24:09', '2022-12-12 09:18:53'),
(54, 2, 'EC1.6.2', 'UE1.6', 2, 2, 1, 0, 2, 'Techniques h&ocirc;teli&egrave;res I', 'TECH HOT', 'techniques-hôtelières-i', '14', '2', 1, 0, 0, '2022-12-09 11:25:32', '2022-12-12 09:19:20'),
(55, 2, 'EC1.5.1', 'UE1.5', 2, 3, 1, 0, 3, 'Droit douanier I', 'DT DOUAN', 'droit-douanier-i', '14', '2', 1, 0, 0, '2022-12-09 11:27:36', '2022-12-12 09:38:41'),
(56, 2, 'EC1.6.1', 'UE1.6', 2, 3, 1, 0, 3, 'Droit constitutionnel', 'DT CONST', 'droit-constitutionnel', '14', '2', 1, 0, 0, '2022-12-09 11:28:32', '2022-12-12 09:39:09'),
(57, 2, 'EC1.6.2', 'UE1.6', 2, 3, 1, 0, 3, 'Histoire des institutions', 'HI', 'histoire-des-institutions', '14', '2', 1, 0, 0, '2022-12-09 11:30:19', '2022-12-12 09:39:42'),
(58, 2, 'EC1.5.1', 'UE1.5', 2, 4, 1, 0, 4, 'Algorithmique et programmation', 'ALGO', 'algorithmique-et-programmation', '21', '3', 1, 0, 0, '2022-12-09 11:48:48', '2022-12-12 10:01:05'),
(59, 2, 'EC1.5.2', 'UE1.5', 2, 4, 1, 0, 4, 'Graphe et optimisation', 'GO', 'graphe-et-optimisation', '14', '2', 1, 0, 0, '2022-12-09 11:50:25', '2022-12-12 10:01:31'),
(60, 2, 'EC1.6.1', 'UE1.6', 2, 4, 1, 0, 4, 'Architecture des machines', 'ARCHI', 'architecture-des-machines', '21', '3', 1, 0, 0, '2022-12-09 12:57:01', '2022-12-12 10:02:13'),
(61, 2, 'EC1.5.1', 'UE1.5', 2, 5, 1, 0, 5, 'Droit douanier I', 'DT DOUAN', 'droit-douanier-i', '35', '5', 1, 0, 0, '2022-12-09 12:59:55', '2022-12-12 10:30:45'),
(62, 2, 'EC1.6.1', 'UE1.6', 2, 5, 1, 0, 5, 'Initiation au marketing', 'I MARKT', 'initiation-au-marketing', '21', '3', 1, 0, 0, '2022-12-09 13:00:42', '2022-12-12 10:32:39'),
(63, 2, 'EC1.5.1', 'UE1.5', 2, 6, 1, 0, 6, 'Amortissement et provision I', 'AMORT', 'amortissement-et-provision-i', '21', '3', 1, 0, 0, '2022-12-09 13:05:22', '2022-12-12 10:47:41'),
(64, 2, 'EC1.5.2', 'UE1.5', 2, 6, 1, 0, 6, 'Etude des cas I', 'EC', 'etude-des-cas-i', '21', '3', 1, 0, 0, '2022-12-09 13:06:01', '2022-12-12 10:48:13'),
(65, 2, 'EC1.6.1', 'UE1.6', 2, 6, 1, 0, 6, 'Initiation au marketing', 'I MARKT', 'initiation-au-marketing', '14', '2', 1, 0, 0, '2022-12-09 13:08:10', '2022-12-12 10:48:37'),
(66, 2, 'EC2.1.1', 'UE2.1', 2, 2, 2, 0, 0, 'Organisation d&#039;entreprise', 'ORG-ENT', 'organisation-d\'entreprise', '7', '1', 1, 1, 0, '2022-12-09 13:10:51', '2022-12-12 09:20:07'),
(67, 2, 'EC2.1.1', 'UE2.1', 2, 3, 2, 0, 0, 'Organisation d&#039;entreprise', 'ORG-ENT', 'organisation-d\'entreprise', '7', '1', 1, 1, 0, '2022-12-09 13:10:51', '2022-12-12 09:40:24'),
(68, 2, 'EC2.1.1', 'UE2.1', 2, 4, 2, 0, 0, 'Organisation d&#039;entreprise', 'ORG-ENT', 'organisation-d\'entreprise', '7', '1', 1, 1, 0, '2022-12-09 13:10:51', '2022-12-12 10:02:45'),
(69, 2, 'EC2.1.1', 'UE2.1', 2, 5, 2, 0, 0, 'Organisation d&#039;entreprise', 'ORG-ENT', 'organisation-d\'entreprise', '7', '1', 1, 1, 0, '2022-12-09 13:10:51', '2022-12-12 10:33:13'),
(70, 2, 'EC2.1.1', 'UE2.1', 2, 6, 2, 0, 0, 'Organisation d&#039;entreprise', 'ORG-ENT', 'organisation-d\'entreprise', '7', '1', 1, 1, 0, '2022-12-09 13:10:51', '2022-12-12 10:49:03'),
(71, 2, 'EC2.1.2', 'UE2.1', 2, 2, 2, 0, 0, 'Communication en entreprise', 'COM-ENT', 'communication-en-entreprise', '7', '1', 1, 1, 0, '2022-12-10 10:03:55', '2022-12-12 09:20:41'),
(72, 2, 'EC2.1.2', 'UE2.1', 2, 3, 2, 0, 0, 'Communication en entreprise', 'COM-ENT', 'communication-en-entreprise', '7', '1', 1, 1, 0, '2022-12-10 10:03:55', '2022-12-12 09:40:58'),
(73, 2, 'EC2.1.2', 'UE2.1', 2, 4, 2, 0, 0, 'Communication en entreprise', 'COM-ENT', 'communication-en-entreprise', '7', '1', 1, 1, 0, '2022-12-10 10:03:55', '2022-12-12 10:03:13'),
(74, 2, 'EC2.1.2', 'UE2.1', 2, 5, 2, 0, 0, 'Communication en entreprise', 'COM-ENT', 'communication-en-entreprise', '7', '1', 1, 1, 0, '2022-12-10 10:03:55', '2022-12-12 10:33:48'),
(75, 2, 'EC2.1.2', 'UE2.1', 2, 6, 2, 0, 0, 'Communication en entreprise', 'COM-ENT', 'communication-en-entreprise', '7', '1', 1, 1, 0, '2022-12-10 10:03:55', '2022-12-12 10:49:41'),
(76, 2, 'EC2.1.3', 'UE2.1', 2, 2, 2, 0, 0, 'Culture g&eacute;n&eacute;rale', 'CULT-GE', 'culture-générale', '7', '1', 1, 1, 0, '2022-12-10 10:05:16', '2022-12-12 09:21:16'),
(77, 2, 'EC2.1.3', 'UE2.1', 2, 3, 2, 0, 0, 'Culture g&eacute;n&eacute;rale', 'CULT-GE', 'culture-générale', '7', '1', 1, 1, 0, '2022-12-10 10:05:16', '2022-12-12 09:41:56'),
(78, 2, 'EC2.1.3', 'UE2.1', 2, 4, 2, 0, 0, 'Culture g&eacute;n&eacute;rale', 'CULT-GE', 'culture-générale', '7', '1', 1, 1, 0, '2022-12-10 10:05:16', '2022-12-12 10:03:42'),
(79, 2, 'EC2.1.3', 'UE2.1', 2, 5, 2, 0, 0, 'Culture g&eacute;n&eacute;rale', 'CULT-GE', 'culture-générale', '7', '1', 1, 1, 0, '2022-12-10 10:05:16', '2022-12-12 10:34:26'),
(80, 2, 'EC2.1.3', 'UE2.1', 2, 6, 2, 0, 0, 'Culture g&eacute;n&eacute;rale', 'CULT-GE', 'culture-générale', '7', '1', 1, 1, 0, '2022-12-10 10:05:16', '2022-12-12 10:50:08'),
(81, 2, 'EC2.2.1', 'UE2.2', 2, 2, 2, 0, 0, 'Initiation au management des organisations I', 'IMO', 'initiation-au-management-des-organisations-i', '28', '4', 1, 1, 0, '2022-12-10 10:12:55', '2022-12-12 09:21:46'),
(82, 2, 'EC2.2.1', 'UE2.2', 2, 3, 2, 0, 0, 'Initiation au management des organisations I', 'IMO', 'initiation-au-management-des-organisations-i', '28', '4', 1, 1, 0, '2022-12-10 10:12:55', '2022-12-12 09:42:32'),
(83, 2, 'EC2.2.1', 'UE2.2', 2, 4, 2, 0, 0, 'Initiation au management des organisations I', 'IMO', 'initiation-au-management-des-organisations-i', '28', '4', 1, 1, 0, '2022-12-10 10:12:55', '2022-12-12 10:04:11'),
(84, 2, 'EC2.2.1', 'UE2.2', 2, 5, 2, 0, 0, 'Initiation au management des organisations I', 'IMO', 'initiation-au-management-des-organisations-i', '28', '4', 1, 1, 0, '2022-12-10 10:12:55', '2022-12-12 10:35:06'),
(85, 2, 'EC2.2.1', 'UE2.2', 2, 6, 2, 0, 0, 'Initiation au management des organisations I', 'IMO', 'initiation-au-management-des-organisations-i', '28', '4', 1, 1, 0, '2022-12-10 10:12:55', '2022-12-12 10:50:36'),
(86, 2, 'EC2.3.1', 'UE2.3', 2, 2, 2, 0, 0, 'Bureautique pratique', 'BUR PRAT', 'bureautique-pratique', '14', '2', 1, 0, 0, '2022-12-10 10:18:19', '2022-12-15 07:32:32'),
(87, 2, 'EC2.3.1', 'UE2.3', 2, 3, 2, 0, 0, 'Bureautique pratique', 'BUR PRAT', 'bureautique-pratique', '14', '2', 1, 1, 0, '2022-12-10 10:18:19', '2022-12-15 07:33:12'),
(88, 2, 'EC2.3.1', 'UE2.3', 2, 4, 2, 0, 0, 'Bureautique pratique', 'BUR PRAT', 'bureautique-pratique', '14', '2', 1, 0, 0, '2022-12-10 10:18:19', '2022-12-15 07:34:04'),
(89, 2, 'EC2.3.1', 'UE2.3', 2, 5, 2, 0, 0, 'Bureautique pratique', 'BUR PRAT', 'bureautique-pratique', '14', '2', 1, 0, 0, '2022-12-10 10:18:19', '2022-12-15 09:59:36'),
(90, 2, 'EC2.3.1', 'UE2.3', 2, 6, 2, 0, 0, 'Bureautique pratique', 'BUR PRAT', 'bureautique-pratique', '14', '2', 1, 0, 0, '2022-12-10 10:18:19', '2022-12-15 10:00:32'),
(91, 2, 'EC2.3.2', 'UE2.3', 2, 2, 2, 0, 0, 'Gestion des ressources humaines I', 'GRH', 'gestion-des-ressources-humaines-i', '14', '2', 1, 0, 0, '2022-12-10 10:33:53', '2022-12-12 14:44:42'),
(92, 2, 'EC2.3.2', 'UE2.3', 2, 3, 2, 0, 0, 'Gestion des ressources humaines I', 'GRH', 'gestion-des-ressources-humaines-i', '14', '2', 1, 1, 0, '2022-12-10 10:33:53', '2022-12-12 09:43:42'),
(93, 2, 'EC2.3.2', 'UE2.3', 2, 4, 2, 0, 0, 'Gestion des ressources humaines I', 'GRH', 'gestion-des-ressources-humaines-i', '14', '2', 1, 0, 0, '2022-12-10 10:33:53', '2022-12-12 14:43:07'),
(94, 2, 'EC2.3.2', 'UE2.3', 2, 5, 2, 0, 0, 'Gestion des ressources humaines I', 'GRH', 'gestion-des-ressources-humaines-i', '14', '2', 1, 0, 0, '2022-12-10 10:33:53', '2022-12-12 14:45:32'),
(95, 2, 'EC2.3.2', 'UE2.3', 2, 6, 2, 0, 0, 'Gestion des ressources humaines I', 'GRH', 'gestion-des-ressources-humaines-i', '14', '2', 1, 0, 0, '2022-12-10 10:33:53', '2022-12-12 14:46:23'),
(96, 2, 'EC2.4.1', 'UE2.4', 2, 2, 2, 0, 2, 'Droit commercial', 'DT COM', 'droit-commercial', '21', '3', 1, 0, 0, '2022-12-10 10:35:45', '2022-12-12 09:23:33'),
(97, 2, 'EC2.5.1', 'UE2.5', 2, 2, 2, 0, 2, 'Fran&ccedil;ais h&ocirc;tellerie', 'FRS HOT', 'français-hôtellerie', '14', '2', 1, 0, 0, '2022-12-10 10:36:52', '2022-12-12 09:24:05'),
(98, 2, 'EC2.5.2', 'UE2.5', 2, 2, 2, 0, 2, 'Anglais h&ocirc;tellerie', 'ANG HOT', 'anglais-hôtellerie', '14', '2', 1, 0, 0, '2022-12-10 10:37:47', '2022-12-12 09:25:02'),
(99, 2, 'EC2.6.1', 'UE2.6', 2, 2, 2, 0, 2, 'Tourisme national', 'TOUR NAT', 'tourisme-national', '21', '3', 1, 0, 0, '2022-12-10 10:40:18', '2022-12-12 09:25:37'),
(100, 2, 'EC2.6.2', 'UE2.6', 2, 2, 2, 0, 2, 'Identit&eacute; culturelle', 'ID CULT', 'identité-culturelle', '14', '2', 1, 0, 0, '2022-12-10 10:41:29', '2022-12-12 09:26:06'),
(101, 2, 'EC2.7.1', 'UE2.7', 2, 2, 2, 0, 2, 'Techniques d&rsquo;h&eacute;bergement ', 'TECH HEBERG', 'techniques-d’hébergement', '21', '3', 1, 0, 0, '2022-12-10 10:44:09', '2022-12-12 09:26:36'),
(102, 2, 'EC2.7.2', 'UE2.7', 2, 2, 2, 0, 2, 'Cuisine', 'CUIS', 'cuisine', '7', '1', 1, 0, 0, '2022-12-10 10:45:12', '2022-12-12 09:27:04'),
(103, 2, 'EC2.8.1', 'UE2.8', 2, 2, 2, 0, 2, 'Conf&eacute;rence-Stage-Rapport-Expos&eacute;', 'EXPO', 'conférence-stage-rapport-exposé', '21', '3', 1, 0, 0, '2022-12-10 10:47:08', '2022-12-12 09:27:33'),
(104, 2, 'EC2.4.1', 'UE2.4', 2, 3, 2, 0, 3, 'Droit commercial', 'DT COM', 'droit-commercial', '21', '3', 1, 0, 0, '2022-12-10 11:25:41', '2022-12-12 09:44:20'),
(105, 2, 'EC2.4.2', 'UE2.4', 2, 3, 2, 0, 3, 'Droit de la famille', 'DT FAM', 'droit-de-la-famille', '21', '3', 1, 0, 0, '2022-12-10 11:26:33', '2022-12-12 09:44:48'),
(106, 2, 'EC2.4.3', 'UE2.4', 2, 3, 2, 0, 3, 'Droit des personnes', 'DT PERS', 'droit-des-personnes', '21', '3', 1, 0, 0, '2022-12-10 11:27:16', '2022-12-12 09:45:21'),
(107, 2, 'EC2.5.1', 'UE2.5', 2, 3, 2, 0, 3, 'Droit administratif I', 'DT ADMIN', 'droit-administratif-i', '28', '4', 1, 0, 0, '2022-12-10 11:28:08', '2022-12-12 09:45:51'),
(108, 2, 'EC2.5.2', 'UE2.5', 2, 3, 2, 0, 3, 'Relations internationales I', 'RI', 'relations-internationales-i', '21', '3', 1, 0, 0, '2022-12-10 11:29:01', '2022-12-12 09:46:22'),
(109, 2, 'EC2.6.1', 'UE2.6', 2, 3, 2, 0, 3, 'Conf&eacute;rence-Stage-Rapport-Expos&eacute;', 'EXPO', 'conférence-stage-rapport-exposé', '21', '3', 1, 0, 0, '2022-12-10 11:29:42', '2022-12-12 09:47:03'),
(110, 2, 'EC2.4.1', 'UE2.4', 2, 4, 2, 0, 4, 'Droit commercial', 'DT COM', 'droit-commercial', '14', '2', 1, 0, 0, '2022-12-10 11:33:55', '2022-12-12 10:08:51'),
(111, 2, 'EC2.5.1', 'UE2.5', 2, 4, 2, 0, 4, 'Initiation &agrave; la base des donn&eacute;es', 'IBD', 'initiation-à-la-base-des-données', '35', '5', 1, 0, 0, '2022-12-10 11:35:11', '2022-12-12 10:09:36'),
(112, 2, 'EC2.5.2', 'UE2.5', 2, 4, 2, 0, 4, 'Mod&eacute;lisation et merise', 'MM', 'modélisation-et-merise', '28', '4', 1, 0, 0, '2022-12-10 11:36:06', '2022-12-12 10:10:31'),
(113, 2, 'EC2.6.1', 'UE2.6', 2, 4, 2, 0, 4, 'Conf&eacute;rence-Stage-Rapport-Expos&eacute;', 'EXPO', 'conférence-stage-rapport-exposé', '21', '3', 1, 0, 0, '2022-12-10 11:37:15', '2022-12-12 10:11:04'),
(114, 2, 'EC2.4.1', 'UE2.4', 2, 5, 2, 0, 5, 'Droit commercial', 'DT COM', 'droit-commercial', '14', '2', 1, 0, 0, '2022-12-12 10:39:16', '2022-12-12 10:39:16'),
(115, 2, 'EC2.5.1', 'UE2.5', 2, 5, 2, 0, 5, 'Marketing stratégique', 'MARKT STRAT', 'marketing-stratégique', '14', '2', 1, 0, 0, '2022-12-12 10:40:22', '2022-12-12 10:40:22'),
(116, 2, 'EC2.6.1', 'UE2.6', 2, 5, 2, 0, 5, 'Transit I', 'TRANS', 'transit-i', '49', '7', 1, 0, 0, '2022-12-12 10:41:45', '2022-12-12 10:41:45'),
(117, 2, 'EC2.7.1', 'UE2.7', 2, 5, 2, 0, 5, 'Conférence-Stage-Rapport-Exposé', 'EXPO', 'conférence-stage-rapport-exposé', '21', '3', 1, 0, 0, '2022-12-12 10:42:32', '2022-12-12 10:42:32'),
(118, 2, 'EC2.4.1', 'UE2.4', 2, 6, 2, 0, 6, 'Droit commercial', 'DT COM', 'droit-commercial', '14', '2', 1, 0, 0, '2022-12-12 10:54:19', '2022-12-12 10:54:19'),
(119, 2, 'EC2.5.1', 'UE2.5', 2, 6, 2, 0, 6, 'Marketing strat&eacute;gique', 'MARKT STRAT', 'marketing-stratégique', '14', '2', 1, 0, 0, '2022-12-12 10:55:21', '2022-12-12 10:56:24'),
(120, 2, 'UE2.6.1', 'UE2.6', 2, 6, 2, 0, 6, 'Travaux de fin d&#039;exercice I', 'TFE', 'travaux-de-fin-d\'exercice-i', '14', '2', 1, 0, 0, '2022-12-12 10:57:35', '2022-12-15 07:27:27'),
(121, 2, 'EC2.6.2', 'UE2.6', 2, 6, 2, 0, 6, 'Comptabilité analytique I', 'COMPTA ANALT', 'comptabilité-analytique-i', '21', '3', 1, 0, 0, '2022-12-12 10:59:35', '2022-12-12 10:59:35'),
(122, 2, 'EC2.6.3', 'UE2.6', 2, 6, 2, 0, 6, 'Etude des cas II', 'EC', 'etude-des-cas-ii', '14', '2', 1, 0, 0, '2022-12-12 11:00:33', '2022-12-13 07:19:50'),
(123, 2, 'EC2.7.1', 'UE2.7', 2, 6, 2, 0, 6, 'Conférence-Stage-Rapport-Exposé', 'EXPO', 'conférence-stage-rapport-exposé', '21', '3', 1, 0, 0, '2022-12-12 11:02:30', '2022-12-12 11:02:30'),
(124, 2, 'EC2.3.3', 'UE2.3', 2, 4, 2, 0, 4, 'Mathématique financière I', 'MATH FI', 'mathématique-financière-i', '21', '3', 1, 0, 0, '2022-12-12 15:03:15', '2022-12-12 15:03:15'),
(125, 2, 'EC2.3.4', 'UE2.3', 2, 4, 2, 0, 4, 'Statistique mathématique I', 'STAT MATH', 'statistique-mathématique-i', '14', '2', 1, 0, 0, '2022-12-12 15:04:19', '2022-12-12 15:04:19'),
(126, 2, 'EC2.3.3', 'UE2.3', 2, 5, 2, 0, 5, 'Mathématique financière I', 'MATH FI', 'mathématique-financière-i', '21', '3', 1, 0, 0, '2022-12-12 15:05:11', '2022-12-12 15:05:11'),
(127, 2, 'EC2.3.4', 'UE2.3', 2, 5, 2, 0, 5, 'Statistique mathématique I', 'STAT MATH', 'statistique-mathématique-i', '14', '2', 1, 0, 0, '2022-12-13 07:22:07', '2022-12-13 07:22:07'),
(128, 2, 'EC2.3.3', 'UE2.3', 2, 6, 2, 0, 6, 'Mathématique financière I', 'MATH FI', 'mathématique-financière-i', '21', '3', 1, 0, 0, '2022-12-13 07:23:05', '2022-12-13 07:23:05'),
(129, 2, 'EC2.3.4', 'UE2.3', 2, 6, 2, 0, 6, 'Statistique mathématique I', 'STAT MATH', 'statistique-mathématique-i', '14', '2', 1, 0, 0, '2022-12-13 07:23:43', '2022-12-13 07:23:43'),
(130, 2, 'EC3.1.1', 'UE3.1', 3, 8, 1, 0, 0, 'Français', 'FRS', 'français', '14', '2', 1, 1, 0, '2022-12-13 14:27:31', '2022-12-13 14:27:31'),
(131, 2, 'EC3.1.1', 'UE3.1', 3, 9, 1, 0, 0, 'Français', 'FRS', 'français', '14', '2', 1, 1, 0, '2022-12-13 14:27:31', '2022-12-13 14:27:31'),
(132, 2, 'EC3.1.1', 'UE3.1', 3, 10, 1, 0, 0, 'Français', 'FRS', 'français', '14', '2', 1, 1, 0, '2022-12-13 14:27:31', '2022-12-13 14:27:31'),
(133, 2, 'EC3.1.1', 'UE3.1', 3, 11, 1, 0, 0, 'Français', 'FRS', 'français', '14', '2', 1, 1, 0, '2022-12-13 14:27:31', '2022-12-13 14:27:31'),
(134, 2, 'EC3.1.1', 'UE3.1', 3, 27, 1, 0, 0, 'Français', 'FRS', 'français', '14', '2', 1, 1, 0, '2022-12-13 14:27:31', '2022-12-13 14:27:31'),
(135, 2, 'EC3.1.2', 'UE3.1', 3, 8, 1, 0, 0, 'Anglais', 'ANG', 'anglais', '14', '2', 1, 1, 0, '2022-12-13 14:28:33', '2022-12-13 14:28:33'),
(136, 2, 'EC3.1.2', 'UE3.1', 3, 9, 1, 0, 0, 'Anglais', 'ANG', 'anglais', '14', '2', 1, 1, 0, '2022-12-13 14:28:33', '2022-12-13 14:28:33'),
(137, 2, 'EC3.1.2', 'UE3.1', 3, 10, 1, 0, 0, 'Anglais', 'ANG', 'anglais', '14', '2', 1, 1, 0, '2022-12-13 14:28:33', '2022-12-13 14:28:33'),
(138, 2, 'EC3.1.2', 'UE3.1', 3, 11, 1, 0, 0, 'Anglais', 'ANG', 'anglais', '14', '2', 1, 1, 0, '2022-12-13 14:28:33', '2022-12-13 14:28:33'),
(139, 2, 'EC3.1.2', 'UE3.1', 3, 27, 1, 0, 0, 'Anglais', 'ANG', 'anglais', '14', '2', 1, 1, 0, '2022-12-13 14:28:33', '2022-12-13 14:28:33'),
(140, 2, 'EC3.1.3', 'UE3.1', 3, 8, 1, 0, 0, 'Italien', 'ITL', 'italien', '14', '2', 1, 1, 0, '2022-12-13 14:29:28', '2022-12-13 14:29:28'),
(141, 2, 'EC3.1.3', 'UE3.1', 3, 9, 1, 0, 0, 'Italien', 'ITL', 'italien', '14', '2', 1, 1, 0, '2022-12-13 14:29:28', '2022-12-13 14:29:28'),
(142, 2, 'EC3.1.3', 'UE3.1', 3, 10, 1, 0, 0, 'Italien', 'ITL', 'italien', '14', '2', 1, 1, 0, '2022-12-13 14:29:28', '2022-12-13 14:29:28'),
(143, 2, 'EC3.1.3', 'UE3.1', 3, 11, 1, 0, 0, 'Italien', 'ITL', 'italien', '14', '2', 1, 1, 0, '2022-12-13 14:29:28', '2022-12-13 14:29:28'),
(144, 2, 'EC3.1.3', 'UE3.1', 3, 27, 1, 0, 0, 'Italien', 'ITL', 'italien', '14', '2', 1, 1, 0, '2022-12-13 14:29:28', '2022-12-13 14:29:28'),
(145, 2, 'EC3.2.1', 'UE3.2', 3, 8, 1, 0, 0, 'Economie monetaire', 'ECO MONET', 'economie-monetaire', '14', '2', 1, 1, 0, '2022-12-13 14:31:04', '2022-12-13 14:32:52'),
(146, 2, 'EC3.2.1', 'UE3.2', 3, 9, 1, 0, 0, 'Economie monetaire', 'ECO MONET', 'economie-monetaire', '14', '2', 1, 1, 0, '2022-12-13 14:31:04', '2022-12-13 14:33:13'),
(147, 2, 'EC3.2.1', 'UE3.2', 3, 10, 1, 0, 0, 'Economie monetaire', 'ECO MONET', 'economie-monetaire', '14', '2', 1, 1, 0, '2022-12-13 14:31:04', '2022-12-13 14:33:36'),
(148, 2, 'EC3.2.1', 'UE3.2', 3, 11, 1, 0, 0, 'Economie monetaire', 'ECO MONET', 'economie-monetaire', '14', '2', 1, 1, 0, '2022-12-13 14:31:04', '2022-12-13 14:33:59'),
(149, 2, 'EC3.2.1', 'UE3.2', 3, 27, 1, 0, 0, 'Economie monetaire', 'ECO MONET', 'economie-monetaire', '14', '2', 1, 1, 0, '2022-12-13 14:31:04', '2022-12-13 14:34:21'),
(150, 2, 'EC3.2.2', 'UE3.2', 3, 8, 1, 0, 0, 'Initiation au management des organisations II', 'IMO', 'initiation-au-management-des-organisations-ii', '28', '4', 1, 1, 0, '2022-12-13 14:32:19', '2022-12-13 14:32:19'),
(151, 2, 'EC3.2.2', 'UE3.2', 3, 9, 1, 0, 0, 'Initiation au management des organisations II', 'IMO', 'initiation-au-management-des-organisations-ii', '28', '4', 1, 1, 0, '2022-12-13 14:32:19', '2022-12-13 14:32:19'),
(152, 2, 'EC3.2.2', 'UE3.2', 3, 10, 1, 0, 0, 'Initiation au management des organisations II', 'IMO', 'initiation-au-management-des-organisations-ii', '28', '4', 1, 1, 0, '2022-12-13 14:32:19', '2022-12-13 14:32:19'),
(153, 2, 'EC3.2.2', 'UE3.2', 3, 11, 1, 0, 0, 'Initiation au management des organisations II', 'IMO', 'initiation-au-management-des-organisations-ii', '28', '4', 1, 1, 0, '2022-12-13 14:32:19', '2022-12-13 14:32:19'),
(154, 2, 'EC3.2.2', 'UE3.2', 3, 27, 1, 0, 0, 'Initiation au management des organisations II', 'IMO', 'initiation-au-management-des-organisations-ii', '28', '4', 1, 1, 0, '2022-12-13 14:32:19', '2022-12-13 14:32:19'),
(155, 2, 'EC3.3.1', 'UE3.3', 3, 8, 1, 0, 0, 'Stratégie et qualité', 'STRAT QUALT', 'stratégie-et-qualité', '7', '1', 1, 1, 0, '2022-12-13 15:04:40', '2022-12-13 15:04:40'),
(156, 2, 'EC3.3.1', 'UE3.3', 3, 9, 1, 0, 0, 'Stratégie et qualité', 'STRAT QUALT', 'stratégie-et-qualité', '7', '1', 1, 1, 0, '2022-12-13 15:04:40', '2022-12-13 15:04:40'),
(157, 2, 'EC3.3.1', 'UE3.3', 3, 10, 1, 0, 0, 'Stratégie et qualité', 'STRAT QUALT', 'stratégie-et-qualité', '7', '1', 1, 1, 0, '2022-12-13 15:04:40', '2022-12-13 15:04:40'),
(158, 2, 'EC3.3.1', 'UE3.3', 3, 11, 1, 0, 0, 'Stratégie et qualité', 'STRAT QUALT', 'stratégie-et-qualité', '7', '1', 1, 1, 0, '2022-12-13 15:04:40', '2022-12-13 15:04:40'),
(159, 2, 'EC3.3.1', 'UE3.3', 3, 27, 1, 0, 0, 'Stratégie et qualité', 'STRAT QUALT', 'stratégie-et-qualité', '7', '1', 1, 1, 0, '2022-12-13 15:04:40', '2022-12-13 15:04:40'),
(160, 2, 'EC3.3.2', 'UE3.3', 3, 8, 1, 0, 0, 'Informatique avancé', 'INFO AV', 'informatique-avancé', '21', '3', 1, 1, 0, '2022-12-14 11:13:11', '2022-12-14 11:13:11'),
(161, 2, 'EC3.3.2', 'UE3.3', 3, 9, 1, 0, 0, 'Informatique avancé', 'INFO AV', 'informatique-avancé', '21', '3', 1, 1, 0, '2022-12-14 11:13:11', '2022-12-14 11:13:11'),
(162, 2, 'EC3.3.2', 'UE3.3', 3, 10, 1, 0, 0, 'Informatique avancé', 'INFO AV', 'informatique-avancé', '21', '3', 1, 1, 0, '2022-12-14 11:13:11', '2022-12-14 11:13:11'),
(163, 2, 'EC3.3.2', 'UE3.3', 3, 11, 1, 0, 0, 'Informatique avancé', 'INFO AV', 'informatique-avancé', '21', '3', 1, 1, 0, '2022-12-14 11:13:11', '2022-12-14 11:13:11'),
(164, 2, 'EC3.3.2', 'UE3.3', 3, 27, 1, 0, 0, 'Informatique avancé', 'INFO AV', 'informatique-avancé', '21', '3', 1, 1, 0, '2022-12-14 11:13:11', '2022-12-14 11:13:11'),
(165, 2, 'EC3.3.3', 'UE3.3', 3, 8, 1, 0, 0, 'Techniques des assurances', 'TECH ASS', 'techniques-des-assurances', '14', '2', 1, 1, 0, '2022-12-14 11:14:25', '2022-12-14 11:14:25'),
(166, 2, 'EC3.3.3', 'UE3.3', 3, 9, 1, 0, 0, 'Techniques des assurances', 'TECH ASS', 'techniques-des-assurances', '14', '2', 1, 1, 0, '2022-12-14 11:14:25', '2022-12-14 11:14:25'),
(167, 2, 'EC3.3.3', 'UE3.3', 3, 10, 1, 0, 0, 'Techniques des assurances', 'TECH ASS', 'techniques-des-assurances', '14', '2', 1, 1, 0, '2022-12-14 11:14:25', '2022-12-14 11:14:25'),
(168, 2, 'EC3.3.3', 'UE3.3', 3, 11, 1, 0, 0, 'Techniques des assurances', 'TECH ASS', 'techniques-des-assurances', '14', '2', 1, 1, 0, '2022-12-14 11:14:25', '2022-12-14 11:14:25'),
(169, 2, 'EC3.3.3', 'UE3.3', 3, 27, 1, 0, 0, 'Techniques des assurances', 'TECH ASS', 'techniques-des-assurances', '14', '2', 1, 1, 0, '2022-12-14 11:14:25', '2022-12-14 11:14:25'),
(170, 2, 'EC3.3.4', 'UE3.3', 3, 8, 1, 0, 0, 'Initiation à l\'entrepreneuriat', 'INIT ENTREP', 'initiation-à-l\'entrepreneuriat', '7', '1', 1, 1, 0, '2022-12-14 11:16:03', '2022-12-14 11:16:03'),
(171, 2, 'EC3.3.4', 'UE3.3', 3, 9, 1, 0, 0, 'Initiation à l\'entrepreneuriat', 'INIT ENTREP', 'initiation-à-l\'entrepreneuriat', '7', '1', 1, 1, 0, '2022-12-14 11:16:03', '2022-12-14 11:16:03'),
(172, 2, 'EC3.3.4', 'UE3.3', 3, 10, 1, 0, 0, 'Initiation à l\'entrepreneuriat', 'INIT ENTREP', 'initiation-à-l\'entrepreneuriat', '7', '1', 1, 1, 0, '2022-12-14 11:16:03', '2022-12-14 11:16:03'),
(173, 2, 'EC3.3.4', 'UE3.3', 3, 11, 1, 0, 0, 'Initiation à l\'entrepreneuriat', 'INIT ENTREP', 'initiation-à-l\'entrepreneuriat', '7', '1', 1, 1, 0, '2022-12-14 11:16:03', '2022-12-14 11:16:03'),
(174, 2, 'EC3.3.4', 'UE3.3', 3, 27, 1, 0, 0, 'Initiation à l\'entrepreneuriat', 'INIT ENTREP', 'initiation-à-l\'entrepreneuriat', '7', '1', 1, 1, 0, '2022-12-14 11:16:03', '2022-12-14 11:16:03'),
(175, 2, 'EC4.1.1', 'UE4.1', 3, 8, 2, 0, 0, 'Entrepreneuriat et gestion de projet', 'ENT GEST PRO', 'entrepreneuriat-et-gestion-de-projet', '7', '1', 1, 1, 0, '2022-12-14 11:19:12', '2022-12-14 11:19:12'),
(176, 2, 'EC4.1.1', 'UE4.1', 3, 9, 2, 0, 0, 'Entrepreneuriat et gestion de projet', 'ENT GEST PRO', 'entrepreneuriat-et-gestion-de-projet', '7', '1', 1, 1, 0, '2022-12-14 11:19:12', '2022-12-14 11:19:12'),
(177, 2, 'EC4.1.1', 'UE4.1', 3, 10, 2, 0, 0, 'Entrepreneuriat et gestion de projet', 'ENT GEST PRO', 'entrepreneuriat-et-gestion-de-projet', '7', '1', 1, 1, 0, '2022-12-14 11:19:12', '2022-12-14 11:19:12'),
(178, 2, 'EC4.1.1', 'UE4.1', 3, 11, 2, 0, 0, 'Entrepreneuriat et gestion de projet', 'ENT GEST PRO', 'entrepreneuriat-et-gestion-de-projet', '7', '1', 1, 1, 0, '2022-12-14 11:19:12', '2022-12-14 11:19:12'),
(179, 2, 'EC4.1.1', 'UE4.1', 3, 27, 2, 0, 0, 'Entrepreneuriat et gestion de projet', 'ENT GEST PRO', 'entrepreneuriat-et-gestion-de-projet', '7', '1', 1, 1, 0, '2022-12-14 11:19:12', '2022-12-14 11:19:12'),
(180, 2, 'EC4.1.2', 'UE4.1', 3, 8, 2, 0, 0, 'Développement personnel et professionnel', 'DEV PERS PRO', 'développement-personnel-et-professionnel', '7', '1', 1, 1, 0, '2022-12-14 11:21:15', '2022-12-14 11:21:15'),
(181, 2, 'EC4.1.2', 'UE4.1', 3, 9, 2, 0, 0, 'Développement personnel et professionnel', 'DEV PERS PRO', 'développement-personnel-et-professionnel', '7', '1', 1, 1, 0, '2022-12-14 11:21:15', '2022-12-14 11:21:15'),
(182, 2, 'EC4.1.2', 'UE4.1', 3, 10, 2, 0, 0, 'Développement personnel et professionnel', 'DEV PERS PRO', 'développement-personnel-et-professionnel', '7', '1', 1, 1, 0, '2022-12-14 11:21:15', '2022-12-14 11:21:15'),
(183, 2, 'EC4.1.2', 'UE4.1', 3, 11, 2, 0, 0, 'Développement personnel et professionnel', 'DEV PERS PRO', 'développement-personnel-et-professionnel', '7', '1', 1, 1, 0, '2022-12-14 11:21:15', '2022-12-14 11:21:15'),
(184, 2, 'EC4.1.2', 'UE4.1', 3, 27, 2, 0, 0, 'Développement personnel et professionnel', 'DEV PERS PRO', 'développement-personnel-et-professionnel', '7', '1', 1, 1, 0, '2022-12-14 11:21:15', '2022-12-14 11:21:15'),
(185, 2, 'EC4.1.3', 'UE4.1', 3, 8, 2, 0, 0, 'Techniques d\'expression', 'TECH EXP', 'techniques-d\'expression', '7', '1', 1, 1, 0, '2022-12-14 11:22:28', '2022-12-14 11:22:28'),
(186, 2, 'EC4.1.3', 'UE4.1', 3, 9, 2, 0, 0, 'Techniques d\'expression', 'TECH EXP', 'techniques-d\'expression', '7', '1', 1, 1, 0, '2022-12-14 11:22:28', '2022-12-14 11:22:28'),
(187, 2, 'EC4.1.3', 'UE4.1', 3, 10, 2, 0, 0, 'Techniques d\'expression', 'TECH EXP', 'techniques-d\'expression', '7', '1', 1, 1, 0, '2022-12-14 11:22:28', '2022-12-14 11:22:28'),
(188, 2, 'EC4.1.3', 'UE4.1', 3, 11, 2, 0, 0, 'Techniques d\'expression', 'TECH EXP', 'techniques-d\'expression', '7', '1', 1, 1, 0, '2022-12-14 11:22:28', '2022-12-14 11:22:28'),
(189, 2, 'EC4.1.3', 'UE4.1', 3, 27, 2, 0, 0, 'Techniques d\'expression', 'TECH EXP', 'techniques-d\'expression', '7', '1', 1, 1, 0, '2022-12-14 11:22:28', '2022-12-14 11:22:28'),
(190, 2, 'EC4.2.1', 'UE4.2', 3, 8, 2, 0, 0, 'Institutions financi&egrave;res', 'INST FIN', 'institutions-financières', '14', '2', 1, 1, 0, '2022-12-14 11:23:38', '2022-12-14 11:58:44'),
(191, 2, 'EC4.2.1', 'UE4.2', 3, 9, 2, 0, 0, 'Institutions financi&egrave;res', 'INST FIN', 'institutions-financières', '14', '2', 1, 1, 0, '2022-12-14 11:23:38', '2022-12-14 11:59:06'),
(192, 2, 'EC4.2.1', 'UE4.2', 3, 10, 2, 0, 0, 'Institutions financi&egrave;res', 'INST FIN', 'institutions-financières', '14', '2', 1, 1, 0, '2022-12-14 11:23:38', '2022-12-14 11:59:27'),
(193, 2, 'EC4.2.1', 'UE4.2', 3, 11, 2, 0, 0, 'Institutions financi&egrave;res', 'INST FIN', 'institutions-financières', '14', '2', 1, 1, 0, '2022-12-14 11:23:38', '2022-12-14 11:59:50'),
(194, 2, 'EC4.2.1', 'UE4.2', 3, 27, 2, 0, 0, 'Institutions financi&egrave;res', 'INST FIN', 'institutions-financières', '14', '2', 1, 1, 0, '2022-12-14 11:23:38', '2022-12-14 11:56:59'),
(195, 2, 'EC4.4.1', 'UE4.4', 3, 8, 2, 0, 0, 'Droit fiscal', 'DT FISC', 'droit-fiscal', '14', '2', 1, 1, 0, '2022-12-14 11:25:06', '2022-12-14 11:25:06'),
(196, 2, 'EC4.4.1', 'UE4.4', 3, 9, 2, 0, 0, 'Droit fiscal', 'DT FISC', 'droit-fiscal', '14', '2', 1, 1, 0, '2022-12-14 11:25:06', '2022-12-14 11:25:06'),
(197, 2, 'EC4.4.1', 'UE4.4', 3, 10, 2, 0, 0, 'Droit fiscal', 'DT FISC', 'droit-fiscal', '14', '2', 1, 1, 0, '2022-12-14 11:25:06', '2022-12-14 11:25:06'),
(198, 2, 'EC4.4.1', 'UE4.4', 3, 11, 2, 0, 0, 'Droit fiscal', 'DT FISC', 'droit-fiscal', '14', '2', 1, 1, 0, '2022-12-14 11:25:06', '2022-12-14 11:25:06'),
(199, 2, 'EC4.4.1', 'UE4.4', 3, 27, 2, 0, 0, 'Droit fiscal', 'DT FISC', 'droit-fiscal', '14', '2', 1, 1, 0, '2022-12-14 11:25:06', '2022-12-14 11:25:06'),
(200, 2, 'EC3.4.1', 'UE3.4', 3, 27, 1, 0, 27, 'Droit social', 'DT SOCIAL', 'droit-social', '14', '2', 1, 0, 0, '2022-12-14 11:27:19', '2022-12-14 11:27:19'),
(201, 2, 'EC3.5.1', 'UE3.5', 3, 27, 1, 0, 27, 'Aménagement touristique II', 'AM TOUR', 'aménagement-touristique-ii', '28', '4', 1, 0, 0, '2022-12-14 11:28:17', '2022-12-14 11:28:17'),
(202, 2, 'EC3.6.1', 'UE3.6', 3, 27, 1, 0, 27, 'Techniques hôtelières II', 'TECH HOT', 'techniques-hôtelières-ii', '35', '5', 1, 0, 0, '2022-12-14 11:29:08', '2022-12-14 11:29:08'),
(203, 2, 'EC4.3.1', 'UE4.3', 3, 27, 2, 0, 27, 'Comptabilité générale II', 'COMPTA GE', 'comptabilité-générale-ii', '21', '3', 1, 0, 0, '2022-12-14 11:38:15', '2022-12-14 11:38:15'),
(204, 2, 'EC4.3.2', 'UE4.3', 3, 27, 2, 0, 27, 'Comptabilité appliqué en informatique (SAGE SAARI)', 'SAGE SAARI', 'comptabilité-appliqué-en-informatique-(sage-saari)', '14', '2', 1, 0, 0, '2022-12-14 11:45:57', '2022-12-14 11:45:57'),
(205, 2, 'EC4.3.3', 'UE4.3', 3, 27, 2, 0, 27, 'Gestion des ressources humaines II', 'GRH', 'gestion-des-ressources-humaines-ii', '14', '2', 1, 0, 0, '2022-12-14 11:47:10', '2022-12-14 11:47:10'),
(206, 2, 'EC4.5.1', 'UE4.5', 3, 27, 2, 0, 27, 'Montage numérique et brochure', 'MONT NUM BRO', 'montage-numérique-et-brochure', '28', '4', 1, 0, 0, '2022-12-14 11:49:03', '2022-12-14 11:49:03'),
(207, 2, 'EC4.5.2', 'UE4.5', 3, 27, 2, 0, 27, 'E-Marketing', 'E MARKT', 'e-marketing', '21', '3', 1, 0, 0, '2022-12-14 11:51:10', '2022-12-14 11:51:10'),
(208, 2, 'EC4.6.1', 'UE4.6', 3, 27, 2, 0, 27, 'Accueil et réception', 'AC RECEP', 'accueil-et-réception', '35', '5', 1, 0, 0, '2022-12-14 11:52:19', '2022-12-14 11:52:19'),
(209, 2, 'EC4.7.1', 'UE4.7', 3, 27, 2, 0, 27, 'Conférence-Stage-Mémoire-Soutenance', 'SOUT', 'conférence-stage-mémoire-soutenance', '28', '4', 1, 0, 0, '2022-12-14 11:54:18', '2022-12-14 11:54:18'),
(210, 2, 'EC3.4.1', 'UE3.4', 3, 8, 1, 0, 8, 'Droit social', 'DT SOCIAL', 'droit-social', '14', '2', 1, 0, 0, '2022-12-14 12:07:35', '2022-12-14 12:07:35'),
(211, 2, 'EC3.4.2', 'UE3.4', 3, 8, 1, 0, 8, 'Droit des assurances', 'DT ASS', 'droit-des-assurances', '28', '4', 1, 0, 0, '2022-12-14 12:08:25', '2022-12-14 12:08:25'),
(212, 2, 'EC3.5.1', 'UE3.5', 3, 8, 1, 0, 8, 'Droit douanier II', 'DT DOUAN', 'droit-douanier-ii', '14', '2', 1, 0, 0, '2022-12-15 05:12:02', '2022-12-15 05:12:02'),
(213, 2, 'EC3.6.1', 'UE3.6', 3, 8, 1, 0, 8, 'Finances publiques', 'FIN PUB', 'finances-publiques', '14', '2', 1, 0, 0, '2022-12-15 05:13:33', '2022-12-15 05:13:33'),
(214, 2, 'EC3.6.2', 'UE3.6', 3, 8, 1, 0, 8, 'Marché public', 'MARCH PUB', 'marché-public', '7', '1', 1, 0, 0, '2022-12-15 05:14:38', '2022-12-15 05:14:38'),
(215, 2, 'EC4.3.1', 'UE4.3', 3, 8, 2, 0, 8, 'Comptabilité générale II', 'COMPTA GE', 'comptabilité-générale-ii', '21', '3', 1, 0, 0, '2022-12-15 05:16:31', '2022-12-15 05:16:31'),
(216, 2, 'EC4.3.2', 'UE4.3', 3, 8, 2, 0, 8, 'Comptabilit&eacute; appliqu&eacute; en informatique (SAGE SAARI)', 'SAGE SAARI', 'comptabilité-appliqué-en-informatique-(sage-saari)', '14', '2', 1, 0, 0, '2022-12-15 05:17:43', '2022-12-21 20:32:20'),
(217, 2, 'EC4.3.3', 'UE4.3', 3, 8, 2, 0, 8, 'Gestion des ressources humaines II', 'GRH', 'gestion-des-ressources-humaines-ii', '14', '2', 1, 0, 0, '2022-12-15 05:21:51', '2022-12-15 05:21:51'),
(218, 2, 'EC4.5.1', 'UE4.5', 3, 8, 2, 0, 8, 'Montage numérique et brochure', 'MONT NUM BRO', 'montage-numérique-et-brochure', '28', '4', 1, 0, 0, '2022-12-15 05:22:57', '2022-12-15 05:22:57'),
(219, 2, 'EC4.6.1', 'UE4.6', 3, 8, 2, 0, 8, 'Droit administratif II', 'DT ADMIN', 'droit-administratif-ii', '21', '3', 1, 0, 0, '2022-12-15 05:23:45', '2022-12-15 05:23:45'),
(220, 2, 'EC4.6.2', 'UE4.6', 3, 8, 2, 0, 8, 'Grands services publics', 'GSP', 'grands-services-publics', '28', '4', 1, 0, 0, '2022-12-15 05:24:28', '2022-12-15 05:24:28'),
(221, 2, 'EC4.6.3', 'UE4.6', 3, 8, 2, 0, 8, 'Relations internationales II', 'RI', 'relations-internationales-ii', '21', '3', 1, 0, 0, '2022-12-15 05:27:04', '2022-12-15 05:27:04'),
(222, 2, 'EC4.7.1', 'UE4.6', 3, 8, 2, 0, 8, 'Conférence-Stage-Mémoire-Soutenance', 'SOUT', 'conférence-stage-mémoire-soutenance', '28', '4', 1, 0, 0, '2022-12-15 05:28:13', '2022-12-15 05:28:13'),
(223, 2, 'EC3.4.1', 'UE3.4', 3, 9, 1, 0, 9, 'Droit social', 'DT SOCIAL', 'droit-social', '14', '2', 1, 0, 0, '2022-12-15 05:30:10', '2022-12-15 05:30:10'),
(224, 2, 'UE3.5.1', 'UE3.5', 3, 9, 1, 0, 9, 'Maintenances des systèmes informatiques', 'MSI', 'maintenances-des-systèmes-informatiques', '28', '4', 1, 0, 0, '2022-12-15 05:31:10', '2022-12-15 05:31:10'),
(225, 2, 'EC3.6.1', 'UE3.6', 3, 9, 1, 0, 9, 'Développement \'application des bases de données', 'DABD', 'développement-\'application-des-bases-de-données', '14', '2', 1, 0, 0, '2022-12-15 05:32:10', '2022-12-15 05:32:10'),
(226, 2, 'EC3.6.2', 'UE3.6', 3, 9, 1, 0, 9, 'HTML', 'HTML', 'html', '21', '3', 1, 0, 0, '2022-12-15 05:32:56', '2022-12-15 05:32:56'),
(227, 2, 'EC4.3.1', 'UE4.3', 3, 9, 2, 0, 9, 'Comptabilité générale II', 'COMPTA GE', 'comptabilité-générale-ii', '21', '3', 1, 0, 0, '2022-12-15 05:33:53', '2022-12-15 05:33:53'),
(228, 2, 'EC4.3.2', 'UE4.3', 3, 9, 2, 0, 9, 'Comptabilité appliqué en informatique (SAGE SAARI)', 'SAGE SAARI', 'comptabilité-appliqué-en-informatique-(sage-saari)', '14', '2', 1, 0, 0, '2022-12-15 05:34:38', '2022-12-15 05:34:38'),
(229, 2, 'EC4.3.3', 'UE4.3', 3, 9, 2, 0, 9, 'Gestion des ressources humaines II', 'GRH', 'gestion-des-ressources-humaines-ii', '14', '2', 1, 0, 0, '2022-12-15 05:35:41', '2022-12-15 05:35:41'),
(230, 2, 'EC4.3.4', 'UE4.3', 3, 9, 2, 0, 9, 'Mathématique financière II', 'MATH FI', 'mathématique-financière-ii', '14', '2', 1, 0, 0, '2022-12-15 05:37:08', '2022-12-15 05:37:08'),
(231, 2, 'EC4.3.5', 'UE4.3', 3, 9, 2, 0, 9, 'Statistique mathématique II', 'STAT MATH', 'statistique-mathématique-ii', '14', '2', 1, 0, 0, '2022-12-15 05:37:46', '2022-12-15 05:37:46'),
(232, 2, 'EC4.5.1', 'UE4.5', 3, 9, 2, 0, 9, 'Montage numérique et brochure', 'MONT NUM BRO', 'montage-numérique-et-brochure', '14', '2', 1, 0, 0, '2022-12-15 05:38:38', '2022-12-15 05:38:38'),
(233, 2, 'EC4.6.1', 'UE4.6', 3, 9, 2, 0, 9, 'Gestion des projets informatiques', 'GPI', 'gestion-des-projets-informatiques', '14', '2', 1, 0, 0, '2022-12-15 05:40:24', '2022-12-15 05:40:24'),
(234, 2, 'EC4.7.1', 'UE4.7', 3, 9, 2, 0, 9, 'Programmation orienté objet', 'POO', 'programmation-orienté-objet', '14', '2', 1, 0, 0, '2022-12-15 05:41:24', '2022-12-15 05:41:24'),
(235, 2, 'EC4.7.2', 'UE4.7', 3, 9, 2, 0, 9, 'ACCES', 'ACCES', 'acces', '14', '2', 1, 0, 0, '2022-12-15 05:42:55', '2022-12-15 05:42:55'),
(236, 2, 'EC4.8.1', 'UE4.8', 3, 9, 2, 0, 9, 'Conférence-Stage-Mémoire-Soutenance', 'SOUT', 'conférence-stage-mémoire-soutenance', '28', '4', 1, 0, 0, '2022-12-15 05:43:29', '2022-12-15 05:43:29'),
(237, 2, 'EC3.4.1', 'UE3.4', 3, 10, 1, 0, 10, 'Droit social', 'DT SOCIAL', 'droit-social', '14', '2', 1, 0, 0, '2022-12-15 05:44:37', '2022-12-15 05:44:37'),
(238, 2, 'EC3.5.1', 'UE3.5', 3, 10, 1, 0, 10, 'Techniques de vente', 'TECH VENT', 'techniques-de-vente', '7', '1', 1, 0, 0, '2022-12-15 05:45:59', '2022-12-15 05:45:59'),
(239, 2, 'EC3.6.1', 'UE3.6', 3, 10, 1, 0, 10, 'Droit douanier II', 'DT DOUAN', 'droit-douanier-ii', '14', '2', 1, 0, 0, '2022-12-15 05:46:50', '2022-12-15 05:46:50'),
(240, 2, 'EC3.7.1', 'UE3.7', 3, 10, 1, 0, 10, 'Marché public', 'MARCH PUB', 'marché-public', '7', '1', 1, 0, 0, '2022-12-15 05:48:38', '2022-12-15 05:48:38'),
(241, 2, 'EC3.7.2', 'UE3.7', 3, 10, 1, 0, 10, 'Management du commerce international', 'MCI', 'management-du-commerce-international', '21', '3', 1, 0, 0, '2022-12-15 05:49:47', '2022-12-15 10:14:29'),
(242, 2, 'EC3.7.3', 'UE3.7', 3, 10, 1, 0, 10, 'Manutention portuaire', 'MANU PORT', 'manutention-portuaire', '14', '2', 1, 0, 0, '2022-12-15 05:50:31', '2022-12-15 10:15:03'),
(243, 2, 'EC4.3.1', 'UE4.3', 3, 10, 2, 0, 10, 'Comptabilité générale II', 'COMPTA GE', 'comptabilité-générale-ii', '21', '3', 1, 0, 0, '2022-12-15 05:51:39', '2022-12-15 05:51:39'),
(244, 2, 'EC4.3.2', 'UE4.3', 3, 10, 2, 0, 10, 'Comptabilité appliqué en informatique (SAGE SAARI)', 'SAGE SAARI', 'comptabilité-appliqué-en-informatique-(sage-saari)', '14', '2', 1, 0, 0, '2022-12-15 05:52:15', '2022-12-15 05:52:15'),
(245, 2, 'ec4;3;3', 'UE4.3', 3, 10, 2, 0, 10, 'Gestion des ressources humaines II', 'GRH', 'gestion-des-ressources-humaines-ii', '14', '2', 1, 0, 0, '2022-12-15 05:52:55', '2022-12-15 05:52:55'),
(246, 2, 'EC4.3.4', 'UE4.3', 3, 10, 2, 0, 10, 'Mathématique financière II', 'MATH FI', 'mathématique-financière-ii', '14', '2', 1, 0, 0, '2022-12-15 05:53:25', '2022-12-15 05:53:25'),
(247, 2, 'EC4.3.5', 'UE4.3', 3, 10, 2, 0, 10, 'Statistique mathématique II', 'STAT MATH', 'statistique-mathématique-ii', '14', '2', 1, 0, 0, '2022-12-15 05:54:01', '2022-12-15 05:54:01'),
(248, 2, 'CE4.5.1', 'UE4.5', 3, 10, 2, 0, 10, 'Montage numérique et brochure', 'MONT NUM BRO', 'montage-numérique-et-brochure', '14', '2', 1, 0, 0, '2022-12-15 05:54:53', '2022-12-15 05:54:53'),
(249, 2, 'EC4.5.2', 'UE4.5', 3, 10, 2, 0, 10, 'E-Marketing', 'E MARKT', 'e-marketing', '21', '3', 1, 0, 0, '2022-12-15 05:55:45', '2022-12-15 05:55:45'),
(250, 2, 'EC4.6.1', 'UE4.6', 3, 10, 2, 0, 10, 'Transit II', 'TRANS', 'transit-ii', '21', '3', 1, 0, 0, '2022-12-15 05:56:27', '2022-12-15 05:56:27'),
(251, 2, 'EC4.7.1', 'UE4.7', 3, 10, 2, 0, 10, 'Conférence-Stage-Mémoire-Soutenance', 'SOUT', 'conférence-stage-mémoire-soutenance', '28', '4', 1, 0, 0, '2022-12-15 05:56:59', '2022-12-15 05:56:59'),
(252, 2, 'EC3.4.1', 'UE3.4', 3, 11, 1, 0, 11, 'Droit social', 'DT SOCIAL', 'droit-social', '14', '2', 1, 0, 0, '2022-12-15 05:58:38', '2022-12-15 05:58:38'),
(253, 2, 'EC3.5.1', 'UE3.5', 3, 11, 1, 0, 11, 'Techniques de vente', 'TECH VENT', 'techniques-de-vente', '7', '1', 1, 0, 0, '2022-12-15 05:59:55', '2022-12-15 05:59:55'),
(254, 2, 'EC3.6.1', 'UE3.6', 3, 11, 1, 0, 11, 'Amortissement et provision II', 'AMORT', 'amortissement-et-provision-ii', '14', '2', 1, 0, 0, '2022-12-15 06:00:51', '2022-12-15 06:00:51'),
(255, 2, 'EC3.6.2', 'UE3.6', 3, 11, 1, 0, 11, 'Etude des cas III', 'EC', 'etude-des-cas-iii', '14', '2', 1, 0, 0, '2022-12-15 06:02:17', '2022-12-15 06:02:17'),
(256, 2, 'EC3.7.1', 'UE3.7', 3, 11, 1, 0, 11, 'Initiation à la comptabilité des coûts', 'ICC', 'initiation-à-la-comptabilité-des-coûts', '14', '2', 1, 0, 0, '2022-12-15 06:03:43', '2022-12-15 06:03:43'),
(257, 2, 'EC3.7.2', 'UE3.7', 3, 11, 1, 0, 11, 'Marché public', 'MARCH PUB', 'marché-public', '7', '1', 1, 0, 0, '2022-12-15 06:04:33', '2022-12-15 06:04:33'),
(258, 2, 'EC3.7.3', 'UE3.7', 3, 11, 1, 0, 11, 'Finances publiques', 'FIN PUB', 'finances-publiques', '7', '1', 1, 0, 0, '2022-12-15 06:05:42', '2022-12-15 06:05:42'),
(259, 2, 'EC4.3.1', 'UE4.3', 3, 11, 2, 0, 11, 'Comptabilité générale II', 'COMPTA GE', 'comptabilité-générale-ii', '21', '3', 1, 0, 0, '2022-12-15 06:06:40', '2022-12-15 06:06:40'),
(260, 2, 'EC4.3.2', 'UE4.3', 3, 11, 2, 0, 11, 'Comptabilité appliqué en informatique (SAGE SAARI)', 'SAGE SAARI', 'comptabilité-appliqué-en-informatique-(sage-saari)', '14', '2', 1, 0, 0, '2022-12-15 06:07:20', '2022-12-15 06:07:20'),
(261, 2, 'EC4.3.3', 'UE4.3', 3, 11, 2, 0, 11, 'Gestion des ressources humaines II', 'GRH', 'gestion-des-ressources-humaines-ii', '14', '2', 1, 0, 0, '2022-12-15 06:08:08', '2022-12-15 06:08:08'),
(262, 2, 'EC4.3.4', 'UE4.3', 3, 11, 2, 0, 11, 'Mathématique financière II', 'MATH FI', 'mathématique-financière-ii', '14', '2', 1, 0, 0, '2022-12-15 06:08:46', '2022-12-15 06:08:46'),
(263, 2, 'EC4.3.5', 'UE4.3', 3, 11, 2, 0, 11, 'Statistique mathématique II', 'STAT MATH', 'statistique-mathématique-ii', '14', '2', 1, 0, 0, '2022-12-15 06:09:23', '2022-12-15 06:09:23'),
(264, 2, 'EC4.5.1', 'UE4.5', 3, 11, 2, 0, 11, 'Montage num&eacute;rique et brochure', 'MONT NUM BRO', 'montage-numérique-et-brochure', '14', '2', 1, 0, 0, '2022-12-15 06:10:35', '2022-12-15 06:17:54'),
(265, 2, 'EC4.6.1', 'UE4.6', 3, 11, 2, 0, 11, 'Comptabilité de trésorerie', 'COMPTA TRE', 'comptabilité-de-trésorerie', '7', '1', 1, 0, 0, '2022-12-15 07:20:22', '2022-12-15 07:20:22'),
(266, 2, 'EC4.6.2', 'UE4.6', 3, 11, 2, 0, 11, 'Analyse financière', 'AN FIN', 'analyse-financière', '14', '2', 1, 0, 0, '2022-12-15 07:21:37', '2022-12-15 07:21:37'),
(267, 2, 'EC4.7.1', 'UE4.7', 3, 11, 2, 0, 11, 'Comptabilité analytique II', 'COMPTA ANALT', 'comptabilité-analytique-ii', '7', '1', 1, 0, 0, '2022-12-15 07:23:49', '2022-12-15 07:23:49'),
(268, 2, 'EC4.7.2', 'UE4.7', 3, 11, 2, 0, 11, 'Etude des cas IV', 'EC', 'etude-des-cas-iv', '7', '1', 1, 0, 0, '2022-12-15 07:24:41', '2022-12-15 07:24:41'),
(269, 2, 'EC4.7.3', 'UE4.7', 3, 11, 2, 0, 11, 'Travaux de fin d\'exercice II', 'TFE', 'travaux-de-fin-d\'exercice-ii', '7', '1', 1, 0, 0, '2022-12-15 07:25:48', '2022-12-15 07:25:48'),
(270, 2, 'EC4.8.1', 'UE4.8', 3, 11, 2, 0, 11, 'Conférence-Stage-Mémoire-Soutenance', 'SOUT', 'conférence-stage-mémoire-soutenance', '28', '4', 1, 0, 0, '2022-12-15 07:26:28', '2022-12-15 07:26:28');

-- --------------------------------------------------------

--
-- Structure de la table `lessons`
--

DROP TABLE IF EXISTS `lessons`;
CREATE TABLE IF NOT EXISTS `lessons` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_student` int UNSIGNED NOT NULL DEFAULT '0',
  `id_cour` int UNSIGNED NOT NULL DEFAULT '0',
  `id_matiere` int UNSIGNED NOT NULL DEFAULT '0',
  `class_id` int UNSIGNED NOT NULL DEFAULT '0',
  `parcour_id` int UNSIGNED NOT NULL DEFAULT '0',
  `vague_id` int UNSIGNED NOT NULL DEFAULT '0',
  `status` int NOT NULL DEFAULT '0',
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_cour` (`id_cour`,`id_matiere`,`class_id`,`parcour_id`,`vague_id`),
  KEY `id_relation_ec` (`id_matiere`),
  KEY `id_relation_class` (`class_id`),
  KEY `id_relation_parcour` (`parcour_id`),
  KEY `id_relation_vague` (`vague_id`),
  KEY `id_student` (`id_student`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `motifspay`
--

DROP TABLE IF EXISTS `motifspay`;
CREATE TABLE IF NOT EXISTS `motifspay` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `motifspay`
--

INSERT INTO `motifspay` (`id`, `title`, `note`, `created_at`, `updated_at`) VALUES
(1, 'Ecolage', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `niveaux`
--

DROP TABLE IF EXISTS `niveaux`;
CREATE TABLE IF NOT EXISTS `niveaux` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int NOT NULL DEFAULT '1',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Déchargement des données de la table `niveaux`
--

INSERT INTO `niveaux` (`id`, `name`, `short`, `status`, `note`, `created_at`, `updated_at`) VALUES
(1, 'Année Préparatoire', 'AP', 1, 'Année Préparatoire', '2022-06-11 13:46:57', '2023-01-06 08:45:11'),
(2, 'Première année', 'L1', 1, 'Diplôme de Technicien Supérieur ', '2022-06-11 13:47:10', '2023-01-06 08:25:21'),
(3, 'Licence 2', 'L2', 1, 'Deuxième année', '2022-06-11 13:47:18', '2023-01-06 08:35:25'),
(4, 'Licence 3', 'L3', 1, 'Troisième année', '2022-06-11 13:47:29', '2023-01-06 08:35:34'),
(5, 'Master 1', 'M1', 1, 'Master professionnel première partie', '2022-06-11 13:47:39', '2023-01-06 08:35:38'),
(6, 'Master 2', 'M2', 1, 'Master professionnel deuxième partie', '2022-06-11 13:47:48', '2023-01-06 08:35:41');

-- --------------------------------------------------------

--
-- Structure de la table `parcours`
--

DROP TABLE IF EXISTS `parcours`;
CREATE TABLE IF NOT EXISTS `parcours` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_id` int UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `parcours`
--

INSERT INTO `parcours` (`id`, `name`, `abr`, `class_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Année Préparatoire', 'AP', 1, 1, '2022-06-11 13:45:27', '2023-01-06 08:42:09'),
(2, 'Tourisme Hôtellerie', 'TH', 2, 1, '2022-06-11 14:01:55', '2023-01-06 08:44:09'),
(3, 'Droit des affaires', 'DR', 2, 1, '2022-06-11 14:03:13', '2022-06-11 14:03:13'),
(4, 'Informatique de Gestion', 'IG', 2, 1, '2022-06-11 14:03:30', '2022-06-11 14:03:30'),
(5, 'Commerce International', 'CI', 2, 1, '2022-06-11 14:03:44', '2022-06-11 14:03:44'),
(6, 'Comptabilité et Finances', 'CF', 2, 1, '2022-06-11 14:04:20', '2023-01-06 08:46:06'),
(8, 'Droit des affaires', 'DR', 3, 1, '2022-06-11 14:05:18', '2022-06-11 14:05:18'),
(9, 'Informatique de Gestion', 'IG', 3, 1, '2022-06-11 14:05:39', '2022-06-11 14:05:39'),
(10, 'Commerce International', 'CI', 3, 1, '2022-06-11 14:05:55', '2022-06-11 14:05:55'),
(11, 'Comptabilit&eacute; et Finances', 'CF', 3, 1, '2022-06-11 14:06:07', '2022-06-11 14:06:07'),
(12, 'Tourisme H&ocirc;tellerie', 'TH', 4, 1, '2022-06-11 14:07:16', '2022-06-11 14:07:16'),
(13, 'Droit des affaires', 'DR', 4, 1, '2022-06-11 14:07:32', '2022-06-11 14:07:32'),
(14, 'Informatique de Gestion', 'IG', 4, 1, '2022-06-11 14:07:52', '2022-06-11 14:07:52'),
(15, 'Commerce International', 'CI', 4, 1, '2022-06-11 14:08:10', '2022-06-11 14:08:10'),
(16, 'Comptabilit&eacute; et Finances', 'CF', 4, 1, '2022-06-11 14:08:31', '2022-06-11 14:08:31'),
(17, 'Tourisme H&ocirc;tellerie', 'TH', 5, 1, '2022-06-11 14:09:00', '2022-06-11 14:09:00'),
(18, 'Droit des affaires', 'DR', 5, 1, '2022-06-11 14:09:10', '2022-06-11 14:09:10'),
(19, 'Informatique de Gestion', 'IG', 5, 1, '2022-06-11 14:09:23', '2022-06-11 14:09:23'),
(20, 'Commerce International', 'CI', 5, 1, '2022-06-11 14:09:36', '2022-06-11 14:09:36'),
(21, 'Comptabilit&eacute; et Finances', 'CF', 5, 1, '2022-06-11 14:09:45', '2022-06-11 14:09:45'),
(22, 'Tourisme H&ocirc;tellerie', 'TH', 6, 1, '2022-06-11 14:10:03', '2022-06-11 14:10:03'),
(23, 'Droit des affaires', 'DR', 6, 1, '2022-06-11 14:10:14', '2022-06-11 14:10:14'),
(24, 'Informatique de Gestion', 'IG', 6, 1, '2022-06-11 14:10:33', '2022-06-11 14:10:33'),
(25, 'Commerce International', 'CI', 6, 1, '2022-06-11 14:10:43', '2022-06-11 14:10:43'),
(26, 'Comptabilit&eacute; et Finances', 'CF', 6, 1, '2022-06-11 14:10:54', '2022-06-11 14:10:54'),
(27, 'Tourisme H&ocirc;tellerie', 'TH', 3, 1, '2022-12-13 11:46:07', '2022-12-13 11:46:07'),
(28, 'Management et Administration des Entreprises', 'MAE', 5, 1, '2022-12-13 14:07:07', '2022-12-13 14:07:07');

-- --------------------------------------------------------

--
-- Structure de la table `password_reminders`
--

DROP TABLE IF EXISTS `password_reminders`;
CREATE TABLE IF NOT EXISTS `password_reminders` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `paycontrol`
--

DROP TABLE IF EXISTS `paycontrol`;
CREATE TABLE IF NOT EXISTS `paycontrol` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_pay` int NOT NULL DEFAULT '0',
  `ecolage` int NOT NULL DEFAULT '0',
  `droit` int NOT NULL DEFAULT '0',
  `otherpay` int NOT NULL DEFAULT '0',
  `mois_reste` tinyint NOT NULL DEFAULT '10',
  `status` int UNSIGNED NOT NULL DEFAULT '0',
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int UNSIGNED NOT NULL DEFAULT '0',
  `id_student` int UNSIGNED NOT NULL,
  `motif` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `montant` int NOT NULL,
  `nbremois` int DEFAULT NULL,
  `agence` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_index` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int UNSIGNED NOT NULL DEFAULT '1',
  `read` int NOT NULL DEFAULT '0',
  `yearsUniv` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `msg` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id_student` (`id_student`),
  KEY `user_id` (`user_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `sexe` int DEFAULT NULL,
  `phone` varchar(14) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover_picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `secretaires`
--

DROP TABLE IF EXISTS `secretaires`;
CREATE TABLE IF NOT EXISTS `secretaires` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NOT NULL DEFAULT '1',
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Déchargement des données de la table `secretaires`
--

INSERT INTO `secretaires` (`id`, `fname`, `lname`, `status`, `token`, `created_at`, `updated_at`) VALUES
(5, 'TOLOTRA', 'OLIVA', 1, 'DRr3tXGkP4mXeq5mtEzrwQen5MPQUEhXW6CBw2Pd', '2023-01-07 23:15:02', '2023-01-07 23:15:02');

-- --------------------------------------------------------

--
-- Structure de la table `semestres`
--

DROP TABLE IF EXISTS `semestres`;
CREATE TABLE IF NOT EXISTS `semestres` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `codeSem` int NOT NULL,
  `semestre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateStart` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dateEnd` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `yearsUniv` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` int NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `semestre_id` (`semestre`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `semestres`
--

INSERT INTO `semestres` (`id`, `codeSem`, `semestre`, `dateStart`, `dateEnd`, `yearsUniv`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Semestre-1', '2022-11-01', '2023-03-15', '2021-2022', 1, '2022-05-24 22:16:09', '2022-12-09 10:24:23'),
(2, 2, 'Semestre-2', '2023-03-20', '2023-07-31', '2021-2022', 1, '2022-05-24 22:17:55', '2022-12-09 10:25:23');

-- --------------------------------------------------------

--
-- Structure de la table `students`
--

DROP TABLE IF EXISTS `students`;
CREATE TABLE IF NOT EXISTS `students` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_id` int UNSIGNED NOT NULL DEFAULT '0',
  `parcour_id` int UNSIGNED NOT NULL DEFAULT '0',
  `vague_id` int UNSIGNED NOT NULL DEFAULT '0',
  `user_id` int UNSIGNED NOT NULL DEFAULT '0',
  `matricule` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `yearsUniv` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int UNSIGNED NOT NULL DEFAULT '0',
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `class_id` (`class_id`,`parcour_id`),
  KEY `vague_id` (`vague_id`),
  KEY `user_id` (`user_id`),
  KEY `id_parcour_student` (`parcour_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Structure de la table `supports`
--

DROP TABLE IF EXISTS `supports`;
CREATE TABLE IF NOT EXISTS `supports` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int UNSIGNED NOT NULL DEFAULT '0',
  `matiere_id` int UNSIGNED NOT NULL,
  `volh` int NOT NULL DEFAULT '0',
  `class_id` int UNSIGNED NOT NULL,
  `parcour_id` int UNSIGNED NOT NULL,
  `vague_id` int UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int NOT NULL DEFAULT '1',
  `yearsUniv` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`matiere_id`,`class_id`,`parcour_id`,`vague_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `tarifs`
--

DROP TABLE IF EXISTS `tarifs`;
CREATE TABLE IF NOT EXISTS `tarifs` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `class_id` int UNSIGNED NOT NULL,
  `ecolage` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `droit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tarifs`
--

INSERT INTO `tarifs` (`id`, `class_id`, `ecolage`, `droit`, `total`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '40000', '80000', '480000', 1, '2022-09-23 11:36:16', '2022-09-23 11:36:16'),
(2, 2, '60000', '80000', '680000', 1, '2022-09-23 11:37:25', '2022-09-23 11:37:25'),
(3, 3, '60000', '80000', '680000', 1, '2022-09-23 11:38:39', '2022-09-23 11:38:39'),
(4, 4, '70000', '80000', '780000', 1, '2022-09-23 11:39:12', '2022-09-23 11:39:12'),
(5, 5, '75000', '80000', '830000', 1, '2022-09-23 11:39:45', '2022-09-23 11:39:45'),
(6, 6, '75000', '80000', '830000', 1, '2022-09-23 11:40:10', '2022-09-23 11:40:10');

-- --------------------------------------------------------

--
-- Structure de la table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
CREATE TABLE IF NOT EXISTS `teachers` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int UNSIGNED NOT NULL DEFAULT '0',
  `status` int NOT NULL DEFAULT '1',
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Déchargement des données de la table `teachers`
--

INSERT INTO `teachers` (`id`, `fname`, `lname`, `user_id`, `status`, `token`, `created_at`, `updated_at`) VALUES
(1, 'BEZARA', 'Florent', 1, 1, 'OW99t3HTa3wHXtWM7MAWjBIwr47KB8BDFSk1mE4i', '2023-01-07 21:43:15', '2023-01-07 21:43:15'),
(2, 'Gasy', 'Coder', 1, 1, 'k3srSohzRgvaaA5f0saWM0NvI4mxUpUUU6tWoVMD', '2023-01-07 21:46:02', '2023-01-07 21:46:02'),
(3, 'Anika Levy', 'Sharon Booker', 1, 1, 'vzZX55ogrfvNsZGnyzOnMLa8N9iNmc4dWuDZ9HBo', '2023-01-07 22:15:04', '2023-01-07 22:15:04'),
(4, 'Roussel Florent', 'BEZARA', 1, 1, 'FavSp2rtAIr7XizroeR1lO596gmDP0Ii3VOktd6D', '2023-01-07 22:15:40', '2023-01-07 22:15:40'),
(5, 'VAVITSARA', 'BEA', 1, 1, 'BY0DYEAqdasPej6GnMpQM9SfnAal6cURfIuEM8rO', '2023-01-07 23:12:50', '2023-01-07 23:12:50');

-- --------------------------------------------------------

--
-- Structure de la table `typespay`
--

DROP TABLE IF EXISTS `typespay`;
CREATE TABLE IF NOT EXISTS `typespay` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `same` int NOT NULL DEFAULT '0',
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int NOT NULL DEFAULT '1',
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `typespay`
--

INSERT INTO `typespay` (`id`, `title`, `same`, `number`, `name`, `icon`, `status`, `token`, `created_at`, `updated_at`) VALUES
(1, 'Telma Mvola', 1, '0349345251', 'BEZARA Florent', '1673560955_images.png', 1, 't2UaHTR1mNiRwkidBBYZd8UK91cAG39vjM8rausQ', '2023-01-08 16:35:33', '2023-01-12 22:02:35'),
(2, 'OrangeMoney', 1, '0329345251', 'BEZARA Florent', '1673195764_orangem.png', 1, 'F9wz6GaTNFuiASGw2enIJyMEFdxZYcYWDiwoYNLJ', '2023-01-08 16:36:03', '2023-01-08 16:36:03'),
(3, 'Airtel Money', 1, '0339345251', 'BEZARA Florent', '1673195785_airtel.png', 1, '98GNY0GvO4L3u6ofubeNPBaNEBG6jB9Ikn3iEyMI', '2023-01-08 16:36:24', '2023-01-13 13:49:40'),
(4, 'Virement Bancaire', 2, '1456213074989', 'BEZARA Florent', '1673195803_banq.png', 0, 'JtKh3pb9wAw8PqR6DmgG1CNkWQLSwAjUCIX5f2cb', '2023-01-08 16:36:42', '2023-01-12 22:02:48'),
(5, 'PayPal', 0, 'bezara@gm.co', 'BEZARA Florent', '1673617922_014.png', 1, '0hDqs8bRLt3sG7x8WqMj2WlhCLon9vnFBr2u1RfZ', '2023-01-10 12:33:16', '2023-01-13 13:52:07');

-- --------------------------------------------------------

--
-- Structure de la table `ues`
--

DROP TABLE IF EXISTS `ues`;
CREATE TABLE IF NOT EXISTS `ues` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int UNSIGNED NOT NULL,
  `codeUe` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `class_id` int UNSIGNED NOT NULL,
  `parcour_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `codeSem` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `al_value` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `credit` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `tronc` tinyint(1) NOT NULL DEFAULT '0',
  `choix` int NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `ues`
--

INSERT INTO `ues` (`id`, `user_id`, `codeUe`, `class_id`, `parcour_id`, `codeSem`, `al_value`, `name`, `slug`, `credit`, `status`, `tronc`, `choix`, `created_at`, `updated_at`) VALUES
(1, 2, 'UE1.1', 2, '2', '1', 0, 'LANGUES VIVANTES', 'langues-vivantes', '6', 1, 1, 1, '2022-12-08 11:21:25', '2022-12-08 11:21:25'),
(2, 2, 'UE1.1', 2, '3', '1', 0, 'LANGUES VIVANTES', 'langues-vivantes', '6', 1, 1, 1, '2022-12-08 11:21:25', '2022-12-08 11:21:25'),
(3, 2, 'UE1.1', 2, '4', '1', 0, 'LANGUES VIVANTES', 'langues-vivantes', '6', 1, 1, 1, '2022-12-08 11:21:25', '2022-12-08 11:21:25'),
(4, 2, 'UE1.1', 2, '5', '1', 0, 'LANGUES VIVANTES', 'langues-vivantes', '6', 1, 1, 1, '2022-12-08 11:21:25', '2022-12-08 11:21:25'),
(5, 2, 'UE1.1', 2, '6', '1', 0, 'LANGUES VIVANTES', 'langues-vivantes', '6', 1, 1, 1, '2022-12-08 11:21:25', '2022-12-08 11:21:25'),
(26, 2, 'UE1.2', 2, '2', '1', 0, 'ECONOMIE ET GESTION', 'economie-et-gestion', '2', 1, 1, 1, '2022-12-08 12:39:01', '2022-12-08 12:39:01'),
(27, 2, 'UE1.2', 2, '3', '1', 0, 'ECONOMIE ET GESTION', 'economie-et-gestion', '2', 1, 1, 1, '2022-12-08 12:39:01', '2022-12-08 12:39:01'),
(28, 2, 'UE1.2', 2, '4', '1', 0, 'ECONOMIE ET GESTION', 'economie-et-gestion', '2', 1, 1, 1, '2022-12-08 12:39:01', '2022-12-08 12:39:01'),
(29, 2, 'UE1.2', 2, '5', '1', 0, 'ECONOMIE ET GESTION', 'economie-et-gestion', '2', 1, 1, 1, '2022-12-08 12:39:01', '2022-12-08 12:39:01'),
(30, 2, 'UE1.2', 2, '6', '1', 0, 'ECONOMIE ET GESTION', 'economie-et-gestion', '2', 1, 1, 1, '2022-12-08 12:39:01', '2022-12-08 12:39:01'),
(31, 2, 'UE1.3', 2, '2', '1', 0, 'OUTILS DE GESTION', 'outils-de-gestion', '8', 1, 1, 1, '2022-12-08 12:41:01', '2022-12-08 12:41:01'),
(32, 2, 'UE1.3', 2, '3', '1', 0, 'OUTILS DE GESTION', 'outils-de-gestion', '8', 1, 1, 1, '2022-12-08 12:41:01', '2022-12-08 12:41:01'),
(33, 2, 'UE1.3', 2, '4', '1', 0, 'OUTILS DE GESTION', 'outils-de-gestion', '8', 1, 1, 1, '2022-12-08 12:41:01', '2022-12-08 12:41:01'),
(34, 2, 'UE1.3', 2, '5', '1', 0, 'OUTILS DE GESTION', 'outils-de-gestion', '8', 1, 1, 1, '2022-12-08 12:41:01', '2022-12-08 12:41:01'),
(35, 2, 'UE1.3', 2, '6', '1', 0, 'OUTILS DE GESTION', 'outils-de-gestion', '8', 1, 1, 1, '2022-12-08 12:41:01', '2022-12-08 12:41:01'),
(36, 2, 'UE1.4', 2, '2', '1', 0, 'DROIT PRIVE', 'droit-prive', '6', 1, 1, 1, '2022-12-08 12:42:29', '2022-12-08 12:42:29'),
(37, 2, 'UE1.4', 2, '3', '1', 0, 'DROIT PRIVE', 'droit-prive', '8', 1, 1, 1, '2022-12-08 12:42:29', '2022-12-08 13:06:23'),
(38, 2, 'UE1.4', 2, '4', '1', 0, 'DROIT PRIVE', 'droit-prive', '6', 1, 1, 1, '2022-12-08 12:42:29', '2022-12-08 13:11:59'),
(39, 2, 'UE1.4', 2, '5', '1', 0, 'DROIT PRIVE', 'droit-prive', '6', 1, 1, 1, '2022-12-08 12:42:29', '2022-12-08 13:12:46'),
(40, 2, 'UE1.4', 2, '6', '1', 0, 'DROIT PRIVE', 'droit-prive', '6', 1, 1, 1, '2022-12-08 12:42:29', '2022-12-08 13:13:15'),
(41, 2, 'UE1.5', 2, '2', '1', 2, 'TOURISME', 'tourisme', '4', 1, 0, 1, '2022-12-08 12:47:24', '2022-12-08 12:47:24'),
(42, 2, 'UE1.6', 2, '2', '1', 2, 'MANAGEMENT DES ACTIVITES HOTELIERES', 'management-des-activites-hotelieres', '4', 1, 0, 1, '2022-12-08 12:48:07', '2022-12-08 12:48:07'),
(43, 2, 'UE1.5', 2, '3', '1', 3, 'DROIT DES DOUANES', 'droit-des-douanes', '2', 1, 0, 1, '2022-12-08 12:49:51', '2022-12-08 12:49:51'),
(44, 2, 'UE1.6', 2, '3', '1', 3, 'DROIT PUBLIC', 'droit-public', '2', 1, 0, 1, '2022-12-08 12:55:49', '2022-12-08 12:55:49'),
(45, 2, 'UE1.5', 2, '4', '1', 4, 'SOFTWARE', 'software', '5', 1, 0, 1, '2022-12-08 12:57:09', '2022-12-08 13:14:18'),
(46, 2, 'UE1.6', 2, '4', '1', 4, 'HARDWARE', 'hardware', '3', 1, 0, 1, '2022-12-08 13:15:02', '2022-12-08 13:15:02'),
(47, 2, 'UE1.5', 2, '5', '1', 5, 'TRANSIT-DOUANE', 'transit-douane', '5', 1, 0, 1, '2022-12-08 13:16:33', '2022-12-08 13:16:33'),
(48, 2, 'UE1.6', 2, '5', '1', 5, 'MARKETING', 'marketing', '3', 1, 0, 1, '2022-12-08 13:17:15', '2022-12-08 13:17:15'),
(49, 2, 'UE1.5', 2, '6', '1', 6, 'COMPTABILITE APPROFONDI', 'comptabilite-approfondi', '6', 1, 0, 1, '2022-12-08 13:20:09', '2022-12-08 13:20:09'),
(50, 2, 'UE1.6', 2, '6', '1', 6, 'MARKETING', 'marketing', '2', 1, 0, 1, '2022-12-08 13:20:40', '2022-12-08 13:20:40'),
(51, 2, 'UE2.1', 2, '2', '2', 0, 'COMMUNICATION ET CULTURE GENERALE', 'communication-et-culture-generale', '3', 1, 1, 1, '2022-12-08 13:32:40', '2022-12-08 13:32:40'),
(52, 2, 'UE2.1', 2, '3', '2', 0, 'COMMUNICATION ET CULTURE GENERALE', 'communication-et-culture-generale', '3', 1, 1, 1, '2022-12-08 13:32:40', '2022-12-08 13:32:40'),
(53, 2, 'UE2.1', 2, '4', '2', 0, 'COMMUNICATION ET CULTURE GENERALE', 'communication-et-culture-generale', '3', 1, 1, 1, '2022-12-08 13:32:40', '2022-12-08 13:32:40'),
(54, 2, 'UE2.1', 2, '5', '2', 0, 'COMMUNICATION ET CULTURE GENERALE', 'communication-et-culture-generale', '3', 1, 1, 1, '2022-12-08 13:32:40', '2022-12-08 13:32:40'),
(55, 2, 'UE2.1', 2, '6', '2', 0, 'COMMUNICATION ET CULTURE GENERALE', 'communication-et-culture-generale', '3', 1, 1, 1, '2022-12-08 13:32:40', '2022-12-08 13:32:40'),
(56, 2, 'UE2.2', 2, '2', '2', 0, 'ECONOMIE ET GESTION 2', 'economie-et-gestion-2', '4', 1, 1, 1, '2022-12-08 13:34:46', '2022-12-08 13:50:05'),
(57, 2, 'UE2.2', 2, '3', '2', 0, 'ECONOMIE ET GESTION 2', 'economie-et-gestion-2', '4', 1, 1, 1, '2022-12-08 13:34:46', '2022-12-08 13:51:11'),
(58, 2, 'UE2.2', 2, '4', '2', 0, 'ECONOMIE ET GESTION 2', 'economie-et-gestion-2', '4', 1, 1, 1, '2022-12-08 13:34:46', '2022-12-08 13:51:43'),
(59, 2, 'UE2.2', 2, '5', '2', 0, 'ECONOMIE ET GESTION 2', 'economie-et-gestion-2', '4', 1, 1, 1, '2022-12-08 13:34:46', '2022-12-08 13:52:51'),
(60, 2, 'UE2.2', 2, '6', '2', 0, 'ECONOMIE ET GESTION 2', 'economie-et-gestion-2', '4', 1, 1, 1, '2022-12-08 13:34:46', '2022-12-08 13:49:08'),
(61, 2, 'UE2.3', 2, '2', '2', 0, 'OUTILS DE GESTION 2', 'outils-de-gestion-2', '4', 1, 0, 1, '2022-12-08 13:35:47', '2022-12-12 14:43:59'),
(63, 2, 'UE2.3', 2, '4', '2', 0, 'OUTILS DE GESTION 2', 'outils-de-gestion-2', '4', 1, 0, 1, '2022-12-08 13:35:47', '2022-12-12 14:41:26'),
(64, 2, 'UE2.3', 2, '5', '2', 0, 'OUTILS DE GESTION 2', 'outils-de-gestion-2', '4', 1, 0, 1, '2022-12-08 13:35:47', '2022-12-12 14:50:10'),
(65, 2, 'UE2.3', 2, '6', '2', 0, 'OUTILS DE GESTION 2', 'outils-de-gestion-2', '4', 1, 0, 1, '2022-12-08 13:35:47', '2022-12-12 14:50:54'),
(71, 2, 'UE2.4', 2, '2', '2', 0, 'DROIT PRIVE 2', 'droit-prive-2', '3', 1, 0, 1, '2022-12-08 13:40:36', '2022-12-08 13:45:41'),
(72, 2, 'UE2.4', 2, '3', '2', 0, 'DROIT PRIVE 2', 'droit-prive-2', '3', 1, 0, 1, '2022-12-08 13:40:36', '2022-12-08 13:46:02'),
(73, 2, 'UE2.4', 2, '4', '2', 0, 'DROIT PRIVE 2', 'droit-prive-2', '3', 1, 0, 1, '2022-12-08 13:40:36', '2022-12-08 13:46:32'),
(74, 2, 'UE2.4', 2, '5', '2', 0, 'DROIT PRIVE 2', 'droit-prive-2', '3', 1, 0, 1, '2022-12-08 13:40:36', '2022-12-08 13:46:47'),
(75, 2, 'UE2.4', 2, '6', '2', 0, 'DROIT PRIVE 2', 'droit-prive-2', '3', 1, 0, 1, '2022-12-08 13:40:36', '2022-12-08 13:47:04'),
(76, 2, 'UE2.5', 2, '4', '2', 4, 'BASES DE DONNEES', 'bases-de-donnees', '9', 1, 0, 1, '2022-12-09 08:36:53', '2022-12-09 08:36:53'),
(77, 2, 'UE2.5', 2, '2', '2', 2, 'LANGUES HOTELIERES', 'langues-hotelieres', '4', 1, 0, 1, '2022-12-09 08:39:12', '2022-12-09 08:39:12'),
(78, 2, 'UE2.6', 2, '2', '2', 2, 'TOURISME 2', 'tourisme', '5', 1, 0, 1, '2022-12-09 08:39:53', '2022-12-10 10:38:49'),
(79, 2, 'UE2.7', 2, '2', '2', 2, 'MANAGEMENT DES ACTIVITES HOTELIERES 2', 'management-des-activites-hotelieres-2', '4', 1, 0, 1, '2022-12-09 08:40:32', '2022-12-09 08:40:32'),
(80, 2, 'UE2.8', 2, '2', '2', 2, 'IMMERSION PROFESSIONNELLE', 'immersion-professionnelle', '3', 1, 0, 1, '2022-12-09 08:56:48', '2022-12-09 08:56:48'),
(81, 2, 'UE2.5', 2, '3', '2', 3, 'DROIT PUBLIC 2', 'droit-public-2', '7', 1, 0, 1, '2022-12-09 09:07:16', '2022-12-09 09:07:16'),
(82, 2, 'UE2.6', 2, '3', '2', 3, 'IMMERSION PROFESSIONNELLE', 'immersion-professionnelle', '3', 1, 0, 1, '2022-12-09 09:08:57', '2022-12-09 09:08:57'),
(83, 2, 'UE2.6', 2, '4', '2', 4, 'IMMERSION PROFESSIONNELLE', 'immersion-professionnelle', '3', 1, 0, 1, '2022-12-09 09:10:17', '2022-12-09 09:10:17'),
(84, 2, 'UE2.5', 2, '5', '2', 5, 'MARKETING 2', 'marketing-2', '2', 1, 0, 1, '2022-12-09 09:11:58', '2022-12-09 09:11:58'),
(85, 2, 'UE2.6', 2, '5', '2', 5, 'TRANSIT-DOUANE 2', 'transit-douane-2', '7', 1, 0, 1, '2022-12-09 09:12:58', '2022-12-09 09:12:58'),
(86, 2, 'UE2.7', 2, '5', '2', 5, 'IMMERSION PROFESSIONNELLE', 'immersion-professionnelle', '3', 1, 0, 1, '2022-12-09 09:13:41', '2022-12-09 09:13:41'),
(87, 2, 'UE2.5', 2, '6', '2', 6, 'MARKETING 2', 'marketing-2', '2', 1, 0, 1, '2022-12-09 09:15:43', '2022-12-09 09:15:43'),
(88, 2, 'UE2.6', 2, '6', '2', 6, 'COMPTABILIT&Eacute; APPROFONDI 2', 'comptabilite-approfondi', '7', 1, 0, 1, '2022-12-09 09:16:38', '2022-12-09 13:03:18'),
(89, 2, 'UE2.7', 2, '6', '2', 6, 'IMMERSION PROFESSIONNELLE', 'immersion-professionnelle', '3', 1, 0, 1, '2022-12-09 09:19:56', '2022-12-09 09:19:56'),
(90, 2, 'UE2.3', 2, '3', '2', 3, 'OUTILS DE GESTION 2', 'outils-de-gestion-2', '4', 1, 0, 1, '2022-12-12 15:01:13', '2022-12-12 15:01:13'),
(91, 2, 'UE3.1', 3, '8', '1', 8, 'LANGUES VIVANTES', 'langues-vivantes', '6', 1, 1, 1, '2022-12-13 07:26:15', '2022-12-13 08:00:09'),
(92, 2, 'UE3.2', 3, '8', '1', 8, 'ECONOMIE ET GESTION', 'economie-et-gestion', '6', 1, 1, 1, '2022-12-13 07:27:07', '2022-12-13 08:00:31'),
(93, 2, 'UE3.3', 3, '8', '1', 8, 'OUTILS DE GESTION', 'outils-de-gestion', '7', 1, 1, 1, '2022-12-13 07:27:49', '2022-12-13 08:00:45'),
(94, 2, 'UE4.4', 3, '8', '2', 8, 'DROIT FISCAL', 'droit-fiscal', '2', 1, 1, 1, '2022-12-13 07:55:45', '2022-12-13 08:00:58'),
(95, 2, 'UE4.1', 3, '8', '2', 8, 'ENTREPRENEURIAT ET EXPRESSION PERSONNEL ET PROFESSIONNEL ', 'entrepreneuriat-et-expression-personnel-et-professionnel', '3', 1, 1, 1, '2022-12-13 07:59:01', '2022-12-13 08:01:12'),
(96, 2, 'UE3.1', 3, '9', '1', 9, 'LANGUES VIVANTES', 'langues-vivantes', '6', 1, 1, 1, '2022-12-13 08:02:18', '2022-12-13 08:03:51'),
(97, 2, 'UE3.2', 3, '9', '1', 9, 'ECONOMIE ET GESTION', 'economie-et-gestion', '6', 1, 1, 1, '2022-12-13 08:02:53', '2022-12-13 08:04:05'),
(98, 2, 'UE3.3', 3, '9', '1', 9, 'OUTILS DE GESTION', 'outils-de-gestion', '7', 1, 1, 1, '2022-12-13 08:03:30', '2022-12-13 08:04:23'),
(99, 2, 'UE4.1', 3, '9', '2', 9, 'ENTREPRENEURIAT ET EXPRESSION PERSONNEL ET PROFESSIONNEL ', 'entrepreneuriat-et-expression-personnel-et-professionnel', '3', 1, 1, 1, '2022-12-13 08:05:03', '2022-12-13 08:06:55'),
(100, 2, 'UE4.2', 3, '9', '2', 9, 'ECONOMIE ET GESTION 2', 'economie-et-gestion-2', '2', 1, 1, 1, '2022-12-13 08:05:43', '2022-12-13 08:07:07'),
(101, 2, 'UE4.4', 3, '9', '2', 9, 'DROIT FISCAL', 'droit-fiscal', '2', 1, 1, 1, '2022-12-13 08:06:39', '2022-12-13 08:07:27'),
(102, 2, 'UE4.2', 3, '8', '2', 8, 'ECONOMIE ET GESTION 2', 'economie-et-gestion-2', '2', 1, 1, 1, '2022-12-13 08:08:58', '2022-12-13 08:09:10'),
(103, 2, 'UE3.1', 3, '10', '1', 10, 'LANGUES VIVANTES', 'langues-vivantes', '6', 1, 1, 1, '2022-12-13 08:10:14', '2022-12-13 08:10:26'),
(104, 2, 'UE3.2', 3, '10', '1', 10, 'ECONOMIE ET GESTION', 'economie-et-gestion', '6', 1, 1, 1, '2022-12-13 08:26:07', '2022-12-13 08:26:18'),
(105, 2, 'UE3.3', 3, '10', '1', 10, 'OUTILS DE GESTION', 'outils-de-gestion', '7', 1, 1, 1, '2022-12-13 08:26:47', '2022-12-13 08:26:58'),
(106, 2, 'UE4.1', 3, '10', '2', 10, 'ENTREPRENEURIAT ET EXPRESSION PERSONNEL ET PROFESSIONNEL ', 'entrepreneuriat-et-expression-personnel-et-professionnel', '3', 1, 1, 1, '2022-12-13 08:27:46', '2022-12-13 08:27:56'),
(107, 2, 'UE4.2', 3, '10', '2', 10, 'ECONOMIE ET GESTION 2', 'economie-et-gestion-2', '2', 1, 1, 1, '2022-12-13 08:28:27', '2022-12-13 08:40:30'),
(108, 2, 'UE4.2', 3, '10', '2', 10, 'ECONOMIE ET GESTION 2', 'economie-et-gestion-2', '', 1, 0, 1, '2022-12-13 08:28:27', '2022-12-13 08:28:27'),
(109, 2, 'UE4.4', 3, '10', '2', 10, 'DROIT FISCAL', 'droit-fiscal', '2', 1, 1, 1, '2022-12-13 08:29:17', '2022-12-13 08:29:31'),
(110, 2, 'UE3.1', 3, '11', '1', 11, 'LANGUES VIVANTES', 'langues-vivantes', '6', 1, 1, 1, '2022-12-13 08:30:42', '2022-12-13 08:32:43'),
(111, 2, 'UE3.1', 3, '11', '1', 11, 'LANGUES VIVANTES', 'langues-vivantes', '3', 1, 0, 1, '2022-12-13 08:31:12', '2022-12-13 08:31:12'),
(112, 2, 'UE3.2', 3, '11', '1', 11, 'ECONOMIE ET GESTION', 'economie-et-gestion', '6', 1, 1, 1, '2022-12-13 08:32:29', '2022-12-13 08:32:56'),
(113, 2, 'UE3.3', 3, '11', '1', 11, 'OUTILS DE GESTION', 'outils-de-gestion', '7', 1, 1, 1, '2022-12-13 08:33:31', '2022-12-13 08:33:47'),
(114, 2, 'UE4.1', 3, '11', '2', 11, 'ENTREPRENEURIAT ET EXPRESSION PERSONNEL ET PROFESSIONNEL ', 'entrepreneuriat-et-expression-personnel-et-professionnel', '3', 1, 1, 1, '2022-12-13 08:34:24', '2022-12-13 08:34:41'),
(115, 2, 'UE4.2', 3, '11', '2', 11, 'ECONOMIE ET GESTION 2', 'economie-et-gestion-2', '2', 1, 1, 1, '2022-12-13 08:37:14', '2022-12-13 08:37:32'),
(116, 2, 'UE4.4', 3, '11', '2', 11, 'DROIT FISCAL', 'droit-fiscal', '2', 1, 1, 1, '2022-12-13 08:39:01', '2022-12-13 08:39:15'),
(117, 2, 'UE3.4', 3, '8', '1', 8, 'DROIT PRIVE', 'droit-prive', '6', 1, 0, 1, '2022-12-13 11:41:21', '2022-12-13 11:41:21'),
(118, 2, 'UE3.5', 3, '8', '1', 8, 'DROIT DES DOUANES', 'droit-des-douanes', '2', 1, 0, 1, '2022-12-13 11:48:46', '2022-12-13 11:48:46'),
(119, 2, 'UE3.6', 3, '8', '1', 8, 'DROIT PUBLIC', 'droit-public', '3', 1, 0, 1, '2022-12-13 11:57:27', '2022-12-13 11:57:27'),
(120, 2, 'UE4.3', 3, '8', '2', 8, 'OUTILS DE GESTION 2', 'outils-de-gestion-2', '7', 1, 0, 1, '2022-12-13 11:58:15', '2022-12-13 11:58:15'),
(121, 2, 'UE4.5', 3, '8', '2', 8, 'MARKETING', 'marketing', '4', 1, 0, 1, '2022-12-13 11:59:15', '2022-12-13 11:59:15'),
(122, 2, 'UE4.6', 3, '8', '2', 8, 'DROIT PUBLIC 2', 'droit-public-2', '10', 1, 0, 1, '2022-12-13 12:00:00', '2022-12-13 12:00:00'),
(123, 2, 'UE4.7', 3, '8', '2', 8, 'IMMERSION PROFESSIONNELLE', 'immersion-professionnelle', '4', 1, 0, 1, '2022-12-13 12:01:05', '2022-12-15 05:28:51'),
(124, 2, 'UE3.4', 3, '9', '1', 9, 'DROIT PRIVE', 'droit-prive', '2', 1, 0, 1, '2022-12-13 12:07:13', '2022-12-13 12:07:13'),
(125, 2, 'UE3.5', 3, '9', '1', 9, 'HARDWARE', 'hardware', '4', 1, 0, 1, '2022-12-13 12:18:55', '2022-12-13 12:18:55'),
(126, 2, 'UE3.6', 3, '9', '1', 9, 'DÉVELOPPEMENT D\'APPLICATION ', 'développement-d\'application', '5', 1, 0, 1, '2022-12-13 12:48:09', '2022-12-13 12:48:09'),
(127, 2, 'UE4.3', 3, '9', '2', 9, 'OUTILS DE GESTION 2', 'outils-de-gestion-2', '11', 1, 0, 1, '2022-12-13 12:48:51', '2022-12-13 12:48:51'),
(128, 2, 'UE4.5', 3, '9', '2', 9, 'MARKETING', 'marketing', '2', 1, 0, 1, '2022-12-13 13:09:18', '2022-12-13 13:09:18'),
(129, 2, 'UE4.6', 3, '9', '2', 9, 'HARDWARE 2', 'hardware', '2', 1, 0, 1, '2022-12-13 13:11:09', '2022-12-15 05:39:24'),
(130, 2, 'UE4.7', 3, '9', '2', 9, 'D&Eacute;VELOPPEMENT D&#039;APPLICATION 2', 'développement-d\'application-2', '4', 1, 0, 1, '2022-12-13 13:12:32', '2022-12-14 11:32:55'),
(131, 2, 'UE4.8', 3, '9', '2', 9, 'IMMERSION PROFESSIONNELLE', 'immersion-professionnelle', '4', 1, 0, 1, '2022-12-13 13:12:54', '2022-12-14 11:33:13'),
(132, 2, 'UE3.4', 3, '10', '1', 10, 'DROIT PRIVE', 'droit-prive', '2', 1, 0, 1, '2022-12-13 13:15:02', '2022-12-13 13:15:02'),
(133, 2, 'UE3.5', 3, '10', '1', 10, 'MARKETING', 'marketing', '1', 1, 0, 1, '2022-12-13 13:18:20', '2022-12-13 13:18:20'),
(134, 2, 'UE3.6', 3, '10', '1', 10, 'TRANSIT-DOUANE', 'transit-douane', '2', 1, 0, 1, '2022-12-13 13:18:53', '2022-12-13 13:18:53'),
(135, 2, 'UE3.7', 3, '10', '1', 10, 'MANAGEMENT DU COMMERCE', 'management-du-commerce', '6', 1, 0, 1, '2022-12-13 13:19:40', '2022-12-13 13:19:40'),
(136, 2, 'UE4.5', 3, '10', '2', 10, 'MARKETING 2', 'marketing-2', '5', 1, 0, 1, '2022-12-13 13:36:23', '2022-12-13 13:36:23'),
(137, 2, 'UE4.6', 3, '10', '2', 10, 'MANAGEMENT DU COMMERCE 2', 'management-du-commerce-2', '3', 1, 0, 1, '2022-12-13 13:37:08', '2022-12-13 13:37:08'),
(138, 2, 'UE4.7', 3, '10', '2', 10, 'IMMERSION PROFESSIONNELLE', 'immersion-professionnelle', '4', 1, 0, 1, '2022-12-13 13:37:41', '2022-12-13 13:37:41'),
(139, 2, 'UE3.4', 3, '11', '1', 11, 'DROIT PRIVE', 'droit-prive', '2', 1, 0, 1, '2022-12-13 13:38:43', '2022-12-13 13:38:43'),
(140, 2, 'UE3.5', 3, '11', '1', 11, 'MARKETING', 'marketing', '1', 1, 0, 1, '2022-12-13 13:39:11', '2022-12-13 13:39:11'),
(141, 2, 'UE3.5', 3, '11', '1', 11, 'MARKETING', 'marketing', '1', 1, 0, 1, '2022-12-13 13:42:39', '2022-12-13 13:42:39'),
(142, 2, 'UE3.6', 3, '11', '1', 11, 'COMPTABILIT&Eacute; APPROFONDI', 'comptabilite-approfondi', '4', 1, 0, 1, '2022-12-13 13:43:21', '2022-12-13 13:51:23'),
(143, 2, 'UE3.7', 3, '11', '1', 11, 'OUTILS COMPLÉMENTAIRES AUX ACTIVITÉS COMPTABLES ET FINANCIÈRES', 'outils-complémentaires-aux-activités-comptables-et-financières', '4', 1, 0, 1, '2022-12-13 13:45:37', '2022-12-13 13:45:37'),
(144, 2, 'UE4.3', 3, '11', '2', 11, 'OUTILS DE GESTION 2', 'outils-de-gestion-2', '11', 1, 0, 1, '2022-12-13 13:46:25', '2022-12-13 13:46:25'),
(145, 2, 'UE4.5', 3, '11', '2', 11, 'MARKETING 2', 'marketing-2', '2', 1, 0, 1, '2022-12-13 13:47:12', '2022-12-13 13:47:12'),
(146, 2, 'UE4.6', 3, '11', '2', 11, 'COMPTABILIT&Eacute; ET ANALYSE', 'comptabilite-et-analyse', '3', 1, 0, 1, '2022-12-13 13:49:19', '2022-12-13 13:51:04'),
(147, 2, 'UE4.6', 3, '11', '2', 11, 'COMPTABILITE ET ANALYSE', 'comptabilite-et-analyse', '3', 1, 0, 1, '2022-12-13 13:49:19', '2022-12-13 13:49:19'),
(148, 2, 'UE4.7', 3, '11', '2', 11, 'COMPTABILITÉ APPROFONDI 2', 'comptabilité-approfondi-2', '3', 1, 0, 1, '2022-12-13 13:50:29', '2022-12-13 13:50:29'),
(149, 2, 'UE4.7', 3, '11', '2', 11, 'COMPTABILITÉ APPROFONDI 2', 'comptabilité-approfondi-2', '3', 1, 0, 1, '2022-12-13 13:50:45', '2022-12-13 13:50:45'),
(150, 2, 'UE4.8', 3, '11', '2', 11, 'IMMERSION PROFESSIONNELLE', 'immersion-professionnelle', '4', 1, 0, 1, '2022-12-13 13:51:54', '2022-12-13 13:51:54'),
(151, 2, 'UE3.1', 3, '27', '1', 27, 'LANGUES VIVANTES', 'langues-vivantes', '6', 1, 1, 1, '2022-12-13 13:52:47', '2022-12-13 14:16:40'),
(152, 2, 'UE3.2', 3, '27', '1', 27, 'ECONOMIE ET GESTION', 'economie-et-gestion', '6', 1, 1, 1, '2022-12-13 14:16:06', '2022-12-13 14:16:52'),
(153, 2, 'UE3.3', 3, '27', '1', 27, 'OUTILS DE GESTION', 'outils-de-gestion', '7', 1, 1, 1, '2022-12-13 14:16:30', '2022-12-13 14:17:06'),
(154, 2, 'UE3.4', 3, '27', '1', 27, 'DROIT PRIVE', 'droit-prive', '2', 1, 0, 1, '2022-12-13 14:17:34', '2022-12-13 14:17:34'),
(155, 2, 'UE3.5', 3, '27', '1', 27, 'TOURISME', 'tourisme', '4', 1, 0, 1, '2022-12-13 14:18:59', '2022-12-13 14:18:59'),
(156, 2, 'UE3.6', 3, '27', '1', 27, 'MANAGEMENT DES ACTIVITES HOTELIERES', 'management-des-activites-hotelieres', '5', 1, 0, 1, '2022-12-13 14:20:03', '2022-12-13 14:20:03'),
(157, 2, 'UE4.1', 3, '27', '2', 27, 'ENTREPRENEURIAT ET EXPRESSION PERSONNEL ET PROFESSIONNEL ', 'entrepreneuriat-et-expression-personnel-et-professionnel', '3', 1, 1, 1, '2022-12-13 14:21:19', '2022-12-13 14:21:46'),
(158, 2, 'UE4.1', 3, '27', '2', 27, 'ENTREPRENEURIAT ET EXPRESSION PERSONNEL ET PROFESSIONNEL ', 'entrepreneuriat-et-expression-personnel-et-professionnel', '3', 1, 0, 1, '2022-12-13 14:21:22', '2022-12-13 14:21:22'),
(159, 2, 'UE4.1', 3, '27', '2', 27, 'ENTREPRENEURIAT ET EXPRESSION PERSONNEL ET PROFESSIONNEL ', 'entrepreneuriat-et-expression-personnel-et-professionnel', '3', 1, 0, 1, '2022-12-13 14:21:28', '2022-12-13 14:21:28'),
(160, 2, 'UE4.2', 3, '27', '2', 27, 'ECONOMIE ET GESTION 2', 'economie-et-gestion', '2', 1, 1, 1, '2022-12-13 14:22:20', '2022-12-14 11:30:25'),
(161, 2, 'UE4.3', 3, '27', '2', 27, 'OUTILS DE GESTION 2', 'outils-de-gestion-2', '7', 1, 0, 1, '2022-12-13 14:23:45', '2022-12-13 14:23:45'),
(162, 2, 'UE4.3', 3, '27', '2', 27, 'OUTILS DE GESTION 2', 'outils-de-gestion-2', '7', 1, 0, 1, '2022-12-13 14:23:49', '2022-12-13 14:23:49'),
(163, 2, 'UE4.4', 3, '27', '2', 27, 'DROIT FISCAL', 'droit-fiscal', '2', 1, 1, 1, '2022-12-13 14:24:23', '2022-12-13 14:24:42'),
(164, 2, 'UE4.5', 3, '27', '2', 27, 'MARKETING', 'marketing', '7', 1, 0, 1, '2022-12-13 14:25:11', '2022-12-13 14:25:11'),
(165, 2, 'UE4.6', 3, '27', '2', 27, 'MANAGEMENT DES ACTIVITES HOTELIERES 2', 'management-des-activites-hotelieres-2', '5', 1, 0, 1, '2022-12-13 14:25:45', '2022-12-15 05:04:54'),
(166, 2, 'UE4.7', 3, '27', '2', 27, 'IMMERSION PROFESSIONNELLE', 'immersion-professionnelle', '4', 1, 0, 1, '2022-12-13 14:26:10', '2022-12-13 14:26:10'),
(167, 4, 'UE 5.1', 4, '12', '1', 0, 'LANGUES VIVANTES', 'langues-vivantes', '3', 1, 1, 1, '2022-12-14 07:22:45', '2022-12-14 07:22:45'),
(168, 4, 'UE 5.1', 4, '13', '1', 0, 'LANGUES VIVANTES', 'langues-vivantes', '3', 1, 1, 1, '2022-12-14 07:22:45', '2022-12-14 07:22:45'),
(169, 4, 'UE 5.1', 4, '14', '1', 0, 'LANGUES VIVANTES', 'langues-vivantes', '3', 1, 1, 1, '2022-12-14 07:22:45', '2022-12-14 07:22:45'),
(170, 4, 'UE 5.1', 4, '15', '1', 0, 'LANGUES VIVANTES', 'langues-vivantes', '3', 1, 1, 1, '2022-12-14 07:22:45', '2022-12-14 07:22:45'),
(171, 4, 'UE 5.1', 4, '16', '1', 0, 'LANGUES VIVANTES', 'langues-vivantes', '3', 1, 1, 1, '2022-12-14 07:22:45', '2022-12-14 07:22:45'),
(172, 2, 'UE4.3', 3, '10', '2', 10, 'OUTILS DE GESTION 2', 'outils-de-gestion-2', '11', 1, 0, 1, '2022-12-14 11:31:49', '2022-12-14 11:31:49'),
(173, 4, 'UE 5.1', 4, '12', '1', 0, 'LANGUES VIVANTES', 'langues-vivantes', '3', 1, 1, 1, '2022-12-14 12:13:14', '2022-12-14 12:13:14'),
(174, 4, 'UE 5.1', 4, '13', '1', 0, 'LANGUES VIVANTES', 'langues-vivantes', '3', 1, 1, 1, '2022-12-14 12:13:14', '2022-12-14 12:13:14'),
(175, 4, 'UE 5.1', 4, '14', '1', 0, 'LANGUES VIVANTES', 'langues-vivantes', '3', 1, 1, 1, '2022-12-14 12:13:14', '2022-12-14 12:13:14'),
(176, 4, 'UE 5.1', 4, '15', '1', 0, 'LANGUES VIVANTES', 'langues-vivantes', '3', 1, 1, 1, '2022-12-14 12:13:14', '2022-12-14 12:13:14'),
(177, 4, 'UE 5.1', 4, '16', '1', 0, 'LANGUES VIVANTES', 'langues-vivantes', '3', 1, 1, 1, '2022-12-14 12:13:14', '2022-12-14 12:13:14'),
(178, 1, 'ssd', 2, '2', '1', 2, 'AlpineJs', 'alpinejs', '5', 1, 0, 1, '2023-01-07 20:53:54', '2023-01-07 20:53:54');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `is_secretaire` tinyint(1) NOT NULL DEFAULT '0',
  `is_teacher` tinyint(1) NOT NULL DEFAULT '0',
  `is_student` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `is_admin`, `is_secretaire`, `is_teacher`, `is_student`, `email`, `password`, `token`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, 0, 'admin@esige.mg', '$2y$10$qh4h05KBbyg/qk.PZySzUu7rVQAGG.dQr8Lus1wjcjvUaJy9.nRs6', '0', 'a5L832RIOy70lKD7RD4ejeMyL7bkdz71eQyFocb3xvFE94QiIGtLZG3oJSpD', '2023-01-07 14:45:53', '2023-01-12 18:32:44');

-- --------------------------------------------------------

--
-- Structure de la table `vagues`
--

DROP TABLE IF EXISTS `vagues`;
CREATE TABLE IF NOT EXISTS `vagues` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `abr` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateStart` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateEnd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int NOT NULL DEFAULT '1',
  `note` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `vagues`
--

INSERT INTO `vagues` (`id`, `name`, `abr`, `dateStart`, `dateEnd`, `status`, `note`, `created_at`, `updated_at`) VALUES
(1, 'Vague1', 'V1', '2023-01-12', '2023-02-20', 1, 'La rentrée de  vague 1 est 12 /01 à 20/02 2023. ', '2022-11-03 10:01:15', '2023-01-13 13:54:52'),
(2, 'Vague2', 'V2', '2023-02-21', '2023-03-20', 1, 'Vague 2 est rentée etsdqsdfdqs', '2022-11-03 10:01:15', '2023-01-11 21:37:56'),
(3, 'Vague3', 'V3', NULL, NULL, 1, NULL, '2022-11-03 10:01:15', '2022-11-03 10:01:15'),
(4, 'Vague4', 'V4', NULL, NULL, 1, NULL, '2022-11-03 10:01:15', '2022-11-03 10:01:15'),
(5, 'Vague5', 'V5', NULL, NULL, 1, NULL, '2022-11-03 10:01:15', '2022-11-03 10:01:15'),
(15, 'Vague 6', 'V6', '2023-05-20', '2023-06-20', 1, 'FBDFDFGSDRF', '2023-01-13 13:53:13', '2023-01-13 13:53:13');

-- --------------------------------------------------------

--
-- Structure de la table `years`
--

DROP TABLE IF EXISTS `years`;
CREATE TABLE IF NOT EXISTS `years` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `yearsUniv` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NOT NULL DEFAULT '1',
  `dateStart` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateEnd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `years`
--

INSERT INTO `years` (`id`, `yearsUniv`, `status`, `dateStart`, `dateEnd`, `created_at`, `updated_at`) VALUES
(1, '2022-2023', 1, '2022-10-27', '2023-10-20', '2022-05-24 22:15:48', '2023-01-10 12:30:26'),
(2, '2023-2024', 0, '2023-10-02', '2024-10-10', '2022-05-24 22:24:19', '2023-01-10 12:30:32');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `parcours`
--
ALTER TABLE `parcours`
  ADD CONSTRAINT `key_class_id` FOREIGN KEY (`class_id`) REFERENCES `niveaux` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
